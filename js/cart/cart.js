function translate() {
	let langCode= document.getElementsByTagName('html')[0].getAttribute('lang').slice(0, 2);
	let phrases = document.getElementsByClassName('translate');
	let toTranslate = [];

	if (phrases.length > 0) {
		for (const phrase of phrases) {
			toTranslate.push(phrase.innerHTML);
		}

		leh_translate(toTranslate, langCode, function(result) {
			if (result) {
				for (const phrase of phrases) {
					for (const translated of result) {
						if (translated) {
							if (phrase.innerHTML == translated.phrase.phrase) {
								phrase.innerHTML = translated.translation;
							}
						}
					}
				}
			}
		});
	}
}

function updateCartIcon(count) {
	var itemCount = document.querySelector('#cartIcon > .itemCount');
	if (itemCount) {
		itemCount.innerHTML = count;
		if (count < 1) {
			itemCount.style.display = 'none';
		} else {
			itemCount.style.display = 'block';
		}
	}
}

function setupCartIcon(count) {
	var cartIcon = document.getElementById('cartIcon');
	if (cartIcon) {
		var itemCount = document.createElement('div');
		itemCount.className = "itemCount";
		cartIcon.appendChild(itemCount);
		updateCartIcon(count);
	}
}

function showCouponInfo(result) {
	var couponInfo = document.getElementById('couponInfo');
	if (couponInfo == null) {
		return;
	}

	var savings = result.total_savings;

	if (result.cpn_status === 'VALID') {
		var content = 'Your exclusive customer discount has been applied.';
		if (savings > 0) {
			content += ' You will save an <span class="couponValue">extra <span class="multi-currency" usdprice="'+savings+'"></span></span> off upon checkout!';
		}
		couponInfo.innerHTML = content;
		couponInfo.style.display = '';
	} else {
		couponInfo.innerHTML = '';
		couponInfo.style.display = 'none';
	}
}

function showFreeShipping(result) {
	var cc 					= leh_var('cc');
	var displayFreeShipping	= false;
	var threshold 			= (result.free_shipping - result.total_price).toFixed(2);
	var convThreshold 		= (convertCurrency(result.cur, result.free_shipping, true) - convertCurrency(result.cur, result.total_price, true)).toFixed(2);

	// Check if the WordPlug options have been enabled.
	if (cc.toUpperCase() == 'US' && wordplugOptions.freeShippingBanner == true) {
		displayFreeShipping	= true;
	}
	if (wordplugOptions.freeIntlShippingBanner == true) {
		displayFreeShipping	= true;
	}

	// If free shipping is enabled.
	if (displayFreeShipping) {
		// 'Free Shipping' will be zero if it is not offered.
		if (result.free_shipping != 0 && result.line_items.length > 0) {
			// Create a container if it does not exist.
			if (jQuery('.ship-info').length == 0) {
				jQuery('#cartBlock').before('<div class="ship-info"></div>');
			}
			// Render the free shipping banner internals.
			if (convThreshold <= 0) {
				jQuery('.ship-info').addClass('free-shippng');
				jQuery('.ship-info').html('<span class="translate">Awesome! You get FREE shipping on your order.</span>');
			} else {
				let statement = 'almost there';
				if (convThreshold < 30) {
					statement = 'so close';
				}
				jQuery('.ship-info').removeClass('free-shippng');
				jQuery('.ship-info').html(`<span class="translate">Congrats, you\'re ` + statement + `!</span> <span class="translate">You are</span> <span>
				` + convertCurrency(result, threshold) + `</span> <span class="translate">away from receiving Free Shipping with your order.</span> <br /><a href="/products/"><span class="translate">Click here to continue shopping!</span></a></em>`);
			}
		}
	}
}

function convertCurrency(result, value, omitSymbol) {
	var currencySymbol 		= '$';
	var currencyRate 		= 1.0;
	var cur 				= leh_var('cur');
	var currencyList 		= leh_get_currency_list();

	if (!cur) {
		cur = 'usd';
	} else {
		cur = cur.toLowerCase();
	}

	if (currencyList && currencyList.length > 0) {
		for (var i=0, n=currencyList.length; i<n; i++) {
			if (cur === currencyList[i].currency.toLowerCase()) {
				currencySymbol = currencyList[i].HTML_code;
				currencyRate = currencyList[i].exchange_rate;
				break;
			}
		}
	}

	// Convert
	var converted = (value / currencyRate).toFixed(2);

	if (omitSymbol) {
		return converted;
	}

	if (cur == 'cad' || cur == 'usd' || cur == 'aud') {
		return currencySymbol + converted;
	} else if (cur == 'eur') {
		return new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(value);
	} else {
		return currencySymbol + converted;
	}
}

(function($) {
	var currencyList = leh_get_currency_list();
	var currentQuantity = 0;

	function matchingString(s, regex) {
		var match = s.match(regex);
		if (match && match.length === 2) {
			return match[1];
		}

		return false;
	}

	function updateAddToCartLink(addToCartButton, addToCartLink, qty, oid, affid) {
		var href = addToCartLink.href;
		if (oid) {
			href = href.replace(/cr=([A-Z0-9]{6})/, 'cr='+oid);
		} else {
			href = href.replace('qty=1', 'qty='+qty);
		}
		if (affid) {
			leh_set('u', ''+affid)
		}
		addToCartButton.href = href;
	}

	function setupQuantity(qty, oid) {
		var productBlock = document.getElementById('productBlock');
		if (productBlock) {
			if ($(productBlock).hasClass('q'+currentQuantity)) {
				$(productBlock).removeClass('q'+currentQuantity);
			}
			$(productBlock).addClass('q'+qty);
			currentQuantity = qty;

			var quantitySelect = productBlock.querySelector('.quantitySelect select');
			if (quantitySelect) {
				$(quantitySelect).val(qty);
			}

			var addToCartButton = productBlock.querySelector('.addToCartButton a');
			var addToCartLink = productBlock.querySelector('.addToCartLink');
			var affid = $(addToCartLink).data('affid');
			if (addToCartButton && addToCartLink) {
				updateAddToCartLink(addToCartButton, addToCartLink, qty, oid, affid);
			}
		}
	}

	var oidString = "";

	function setupPackageSelectionButton(button) {
		var oid = matchingString(button.className, /oid\[([A-Z0-9]{6})\]/);
		if (oid) {
			oidString += oid+",";
		}
		var itemQty = matchingString(button.className, /q([0-9]{1,2})/);
		if (itemQty) {
			$(button).find('a').on('click', function() {
				setupQuantity(itemQty, oid);

				return false;
			});
		}
	}

	function setupQuantitySelect(quantitySelect, qty) {
		var select = document.createElement('select');
		var qtyList = JSON.parse(matchingString(quantitySelect.className, /q(\[[0-9,]+\])/));
		var matchingQty = false;
		if (qtyList && qtyList.length > 0) {
			for (var i=0, n=qtyList.length; i < n; i++) {
				if (qty === qtyList[i]) {
					matchingQty = true;
				}
				var option = new Option(qtyList[i], qtyList[i]);
				select.appendChild(option);
			}
		}
		if (!matchingQty) {
			qty = qtyList[0];
		}
		$(select).on('change', function() {
			setupQuantity($(this).val());
		});

		quantitySelect.innerHTML = '';
		quantitySelect.appendChild(select);

		return qty;
	}

	function setupProductBlock(result) {
		var productBlock = document.getElementById('productBlock');
		if (productBlock) {
			var packageSelectionContainer = productBlock.querySelector('.packageSelectionContainer');
			if (packageSelectionContainer) {
				var packageSelectionButtonList = packageSelectionContainer.getElementsByClassName('quantityButton');
				for (var i = packageSelectionButtonList.length - 1; i >= 0; i--) {
					setupPackageSelectionButton(packageSelectionButtonList[i]);
				}
			}
			var qty = 1;
			if (result && result.total_units && result.line_items) {
				var addToCartLink = productBlock.querySelector('.addToCartLink');
				if (addToCartLink) {
					var cartRef = matchingString(addToCartLink.href, /cr=([A-Z0-9]{6})/);
					if (cartRef) {
						var line_items = result.line_items;
						for (var i = line_items.length - 1; i >= 0; i--) {
							if (line_items[i].original_cart_ref === cartRef) {
								qty = line_items[i].qty;
								break;
							}
						}
					}
				}
			}
			var quantitySelect = productBlock.querySelector('.quantitySelect');
			if (quantitySelect) {
				qty = setupQuantitySelect(quantitySelect, qty);
			}
			if (!setupHashQty()) {
				setupQuantity(qty);
			}
		}
	}

	function setupHashQty() {
		var productBlock = document.getElementById('productBlock');
		var hash = window.location.hash;
		if (hash && productBlock) {
			var qty = /([0-9]+)month/.exec(hash);
			if (qty && qty.length === 2) {
				setupQuantity(qty[1]);

				return true;
			}
		}

		return false;
	}

	function createCurrencySelector(result) {
		var select = document.createElement('select');
		select.setAttribute('autocomplete', 'off');
		if (result.cur) {
			var cur = result.cur.toUpperCase();
		} else {
			var cur = leh_var('cur', 'usd').toUpperCase();
		}
		if (currencyList && currencyList.length > 0) {
			for (var i=0, n=currencyList.length; i<n; i++) {
				var option = document.createElement('option');
				option.innerHTML = currencyList[i].currency.toUpperCase();
				if (cur === option.innerHTML) {
					option.setAttribute('selected', 'selected');
				}
				select.appendChild(option);
			}
		}
		$(select).on('change', function() {
			var selectedIndex = this.selectedIndex;
			var cur = currencyList[selectedIndex].currency.toLowerCase();
			updatePriceValues(currencyList[selectedIndex].exchange_rate, currencyList[selectedIndex].HTML_code, cur);
			updateCheckoutLink(cur);
			leh_set('cur', cur);
			if (result.total_units > 0) {
				showFreeShipping(result);
			}
		});

		return select;
	}

	function updateCheckoutLink(cur) {
		var checkoutLinks = document.querySelectorAll('.cart-container > .summary > .checkout > a, .paypal > a');

		if (checkoutLinks) {
			checkoutLinks.forEach(el => {
				el.href = el.href.replace(/cur=[a-z]{3}/, 'cur='+cur);
			});
		}
	}

	function updatePriceValues(rate, code, cur) {
		var spanList = document.getElementsByClassName('multi-currency');
		cur = cur.toUpperCase();
        if (leh_base_cur != 'usd') {
            // Don't muck with the usd prices here
            rate = 1;
        }
		for (var i = spanList.length - 1; i >= 0; i--) {

			// Parse out pricing object
			if (spanList[i].getAttribute('msrp')) {
				var priceObj = eval(spanList[i].getAttribute('msrp'));
				spanList[i].setAttribute('usdprice',priceObj.msrp);
			}
			if (spanList[i].getAttribute('savings')) {
				var priceObj = eval(spanList[i].getAttribute('savings'));
				spanList[i].setAttribute('usdprice',priceObj.savings);
			}
			if (spanList[i].getAttribute('price')) {
				var priceObj = eval(spanList[i].getAttribute('price'));
				spanList[i].setAttribute('usdprice',priceObj.price);
			}

			var value = spanList[i].getAttribute('usdprice').replace(',', '');
			if (spanList[i].className.indexOf('round-ones') !== -1) {
				value = Math.ceil(value/rate).toFixed(0);
			} else if (spanList[i].className.indexOf('round-tens') !== -1) {
				value = (Math.ceil(value/rate/10.0)*10).toFixed(0);
			} else {
				value = (value/rate).toFixed(2);
			}

			if (cur == 'CAD' || cur == 'USD' || cur == 'AUD') {
				spanList[i].innerHTML = code+value.replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
			}

			if (cur == 'EUR') {
				spanList[i].innerHTML = new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(value);
			}

			if (spanList[i].className.indexOf('show-dollar-curtype') !== -1 && code === '$' && cur !== 'USD') {
				spanList[i].innerHTML += ' ('+cur+')'
			}
		}
	}

	function getFirstElementByClassName(elem, className) {
		var list = elem.getElementsByClassName(className);
		if (list && list.length > 0) {
			return list[0];
		}

		return null;
	}

	function setLoading() {
		var cartBlock = document.getElementById('cartBlock');
		if (cartBlock) {
			var cartContainer = getFirstElementByClassName(cartBlock, 'cart-container');
			if (cartContainer) {
				$(cartContainer).addClass('loading');
			}
		}
	}

	function runCartApi() {
		var func = Array.prototype.shift.apply(arguments);
		Array.prototype.push.call(arguments, function(result) {
			if (result && result.fail) {
				window.location.reload(true);
			} else if (result) {
				if (result.total_units > 0) {
					createCart(result);
					showCouponInfo(result);
					showFreeShipping(result);
				} else {
					createEmptyCart();
					hideCouponInfo();
					updateCartIcon(0);
				}
				setupCurrencyValues(result);
			}
		});
		setLoading();
		func.apply(this, arguments);
	}

	function createQtySelector(line_item) {
		var quantity_options = JSON.parse(line_item.quantity_options);
		if (quantity_options.length) {
			var selectHTML = '<select class="quantity-selector">';
			for (var i=0, n=quantity_options.length; i<n; i++) {
				selectHTML += '<option '+(line_item.qty === quantity_options[i]?'selected="selected" ':'')+'value="'+quantity_options[i]+'">'+quantity_options[i]+'</option>';
			}
			selectHTML += '</select>';

			return selectHTML;
		} else {
			return '<select class="quantity-selector"><option value='+quantity_options+'>'+quantity_options+'</option></select>';
		}
	}

	function createSummaryItems(result) {
		var lineItemContainer = document.querySelector('#cartBlock > .cart-container > .summary > .line-item-container');
		if (lineItemContainer) {
			var lineHTML = '';
			var savings = result.total_savings;

			if (result.total_gross != result.cart_total) {
				lineHTML += '			<div class="line-item">';
				lineHTML += '				<div class="description translate">Subtotal</div>';
				lineHTML += '				<div class="value multi-currency" usdprice="'+result.total_gross+'"></div>';
				lineHTML += '			</div>';
			}
			if (savings > 0) {
				lineHTML += '			<div class="line-item">';
				lineHTML += '				<div class="description translate">Savings</div>';
				lineHTML += '				<div class="value negative multi-currency" usdprice="'+savings+'"></div>';
				lineHTML += '			</div>';
			}
			lineHTML += '			<div class="line-item">';
			lineHTML += '				<div class="title description translate">Total</div>';
			lineHTML += '				<div class="value total multi-currency" usdprice="'+result.cart_total+'"></div>';
			lineHTML += '			</div>';

			lineItemContainer.innerHTML = lineHTML;
		}
	}

	function calculatePercent(amount, percent) {
		const amountDecimals = getNumberOfDecimals(amount);
		const percentDecimals = getNumberOfDecimals(percent);
		const amountAsInteger = Math.round(amount + `e${amountDecimals}`);
		const percentAsInteger = Math.round(percent + `e${percentDecimals}`);
		// add 2 to scale by an additional 100 since the percentage supplied is 100x the actual multiple (e.g. 35.8% is passed as 35.8, but as a proper multiple is 0.358)
		const precisionCorrection = `e-${amountDecimals + percentDecimals + 2}`;

		return Number((amountAsInteger * percentAsInteger) + precisionCorrection);
	}

	function getNumberOfDecimals(number) {
		const decimals = parseFloat(number).toString().split('.')[1];

		if (decimals) {
			return decimals.length;
		}

		return 0;
	}

	function createLineItems(line_items, result) {
		var cartBlock = document.getElementById('cartBlock');
		if (cartBlock) {
			var lineItemContainer = cartBlock.querySelector('#cartBlock > .cart-container > .cart > .line-item-container');
			if (lineItemContainer) {
				lineItemContainer.innerHTML = '';

				if (line_items) {
					for (var i=0, n=line_items.length; i<n; i++) {
						var promo_icon 	= '';
						var award_msg	= '';
						var discount 	= null;
						var free		= false;

						switch(line_items[i].discount) {
							case 'coupon':
								award_msg	= '<div class="coupon">' + result.discount_applied_msg + ' &nbsp;</div>';
								break;
							case 'promo':
								promo_icon 	= '<img width="18" height="18" src="/cart-assets/img/promo-tag.svg" class="svg-promo-tag">';
								award_msg	= '<div class="coupon">' + line_items[i].promo_tag + ' &nbsp;</div>';
								break;
							default:

						}
						if (Number(line_items[i].price) == 0)  {
							free = true;
						}
						var lineItem = document.createElement('div');
						lineItem.className = 'line-item';

						var lineHTML = '';
						lineHTML += '				<div class="column product">';
						lineHTML += '					<div class="icon"><img width="80" height="80" src="/cart-assets/img/products/'+line_items[i].cart_ref+'_400.jpg"></div>';
						lineHTML += '					<div class="product-description">'+award_msg+line_items[i].product_desc+'</div>';
            			if (line_items[i].is_award) {
						lineHTML += '					<div class="r-space"></div>';
            			} else {
						lineHTML += '					<div class="r-space"><div class="remove item">X<span class="long"> <span class="translate">Remove</span></span></div></div>';
						}
						lineHTML += '				</div>';

						// Unit Price
						lineHTML += '				<div class="column middle unit-price">';
						lineHTML += '					<div class="container">';

						if (free)  {

						} else if (discount || line_items[i].gross_price !== line_items[i].price) {
						lineHTML += '						<div class="middle qty"><div>'+createQtySelector(line_items[i])+'&nbsp;x&nbsp;</div></div>';
						lineHTML += '						<div class="middle price-discount-nudge">';
						lineHTML += '							<div class="msrp price multi-currency" usdprice="'+line_items[i].gross_price/line_items[i].qty+'"></div>';
							lineHTML += '							<div class="price multi-currency" usdprice="'+(line_items[i].price/line_items[i].qty)+'"></div>';
						} else {
						lineHTML += '						<div class="middle qty"><div>'+createQtySelector(line_items[i])+'&nbsp;x&nbsp;</div></div>';
						lineHTML += '						<div class="middle">';
						lineHTML += '							<div class="price multi-currency" usdprice="'+line_items[i].unit_price+'"></div>';
						}

						lineHTML += '						</div>';
						lineHTML += '					</div>';
						lineHTML += '				</div>';

						// Subtotal
						lineHTML += '				<div class="column middle subtotal">';
						if (free) {
						lineHTML += '						<p class="free"><em><span class="translate">Free!</span></em></p>';
						} else {
						lineHTML += '						<div class="msrp price multi-currency no-strike" usdprice="'+line_items[i].unit_price*line_items[i].qty+'"></div>';
						// lineHTML += '						<div class="msrp price ui-multi-currency multi-currency no-strike" usdprice="'+line_items[i].unit_price*line_items[i].qty+'"></div>';
						}
						lineHTML += '				</div>';

						lineItem.innerHTML = lineHTML;

						lineItemContainer.appendChild(lineItem);

						(function(cart_ref) {
							$(lineItem).find('.remove.item').on('click', function() {
								runCartApi(leh_remove_product, cart_ref);
							});
							$(lineItem).find('.remove.coupon').on('click', function() {
								runCartApi(leh_remove_coupon);
							});
							$(lineItem).find('select.quantity-selector').on('change', function() {
								runCartApi(leh_update_quantity, cart_ref, this.value);
							});
						})(line_items[i].original_cart_ref);
					}
				}
			}
		}
	}

	function createEmptyCart() {
		var cartBlock = document.getElementById('cartBlock');
		if (cartBlock) {
			cartBlock.innerHTML = "<h1><span class='translate'>Your Cart is Empty</span></h1>"
		}
	}

	function createCart(result) {
		var cartBlock = document.getElementById('cartBlock');
		if (cartBlock) {
			var line_items = result.line_items;

			if (line_items && line_items.length > 0) {
				line_items.sort(function(a, b) {
                    ap = parseFloat(a.gross_price);
                    bp = parseFloat(b.gross_price);
                    if (ap < bp) {
                         return 1;
                    } else if (ap > bp) {
                        return -1;
                    }

					return 0;
				});

				var cartHTML = "";
        		cartHTML += '<div id="couponInfo"></div>';
				cartHTML += '<div class="cart-container">';
				cartHTML += '	<div class="cart">';
				cartHTML += '		<div class="header">';
				cartHTML += '			<div class="column product">';
				cartHTML += '				<img width="13" height="17" src="/cart-assets/img/lock-icon.svg" class="svg-lock">';
				cartHTML += '				<span class="title translate">Your Cart</span>';
				cartHTML += '				<span><span class="translate">Items</span>&nbsp;('+line_items.length+')</span>';
				cartHTML += '				<div class="currency-selector"></div>';
				cartHTML += '			</div>';
				cartHTML += '			<div class="column middle unit-price">';
				cartHTML += '				<div class="price title translate">Unit Price</div>';
				cartHTML += '			</div>';
				cartHTML += '			<div class="column middle subtotal title translate">Subtotal</div>';
				cartHTML += '		</div>';
				cartHTML += '		<div class="line-item-container">';
				cartHTML += '		</div>';
				cartHTML += '	</div>';
				cartHTML += '	<div class="summary">';
				cartHTML += '		<div class="heading translate">Summary</div>';
				cartHTML += '		<div class="line-item-container">';
				cartHTML += '		</div>';
				cartHTML += '		<div class="checkout">';
				cartHTML += '			<a href="'+result.actions.checkout_url+'" class="translate">Checkout Now!</a>';
				cartHTML += '			<a href="/products" class="continue translate">Continue Shopping</a>';
				cartHTML += '		</div>';
				if (wordplugOptions.paypalMethod == true) {
					cartHTML += '		<div class="paypal" style="margin-top:5px;">';
					cartHTML += '			<a href="'+result.actions.checkout_url+'"><img src="/wp-content/plugins/wordplug/assets/img/paypal-button.png"></a>';
					cartHTML += '		</div>';
					cartHTML += '		<div class="paypal" style="margin-bottom:5px;">';
					cartHTML += '			<a href="'+result.actions.checkout_url+'"><img src="/wp-content/plugins/wordplug/assets/img/paypal-credit-button.png"></a>';
					cartHTML += '		</div>';
				}
				cartHTML += '	</div>';
				cartHTML += '</div>';
			}

			cartBlock.innerHTML = cartHTML;
			createLineItems(line_items, result);
			createSummaryItems(result);

			var currencySelectorList = cartBlock.getElementsByClassName('currency-selector');
			for (var i = 0; i < currencySelectorList.length; i++) {
				currencySelectorList[i].appendChild(createCurrencySelector(result));
			}
			if (result.line_items) {
				updateCartIcon(result.line_items.length);
			} else {
				updateCartIcon(0);
			}
		}
	}

	function hideCouponInfo() {
		var couponInfo = document.getElementById('couponInfo');
		if (couponInfo != null) {
			couponInfo.style.display = 'none';
		}
	}

	function showPromoCode(result) {
		var u = leh_var('u', 'None');
		if (result && result.actions && result.actions.checkout_url) {
			var url = result.actions.checkout_url+"";
			var match = /&PT=([0-9]+)/.exec(url);
			if (match) {
				if (match[1]) {
					u = match[1];
				}
			}
		}
		var promoCodeList = document.getElementsByClassName('prc');
		for (var i = promoCodeList.length - 1; i >= 0; i--) {
			promoCodeList[i].innerHTML = promoCodeList[i].innerHTML.replace('None', u);
		}
	}

	function setupMultiCurrency() {
		var multiCurrencyList = document.getElementsByClassName('ui-multi-currency');
		var moneyPattern = /\$([0-9]{1,3}(?:,?[0-9]{3})*(?:\.[0-9]{2})?)/;
		var cur = leh_var('cur', 'usd').toUpperCase();

		for (var i = multiCurrencyList.length - 1; i >= 0; i--) {
			var extraClass = '';
			if (multiCurrencyList[i].className.indexOf('show-dollar-curtype') !== -1) {
				extraClass += ' show-dollar-curtype';
			}
			if (multiCurrencyList[i].className.indexOf('round-ones') !== -1) {
				extraClass += ' round-ones';
			}
			if (multiCurrencyList[i].className.indexOf('round-tens') !== -1) {
				extraClass += ' round-tens';
			}
			multiCurrencyList[i].innerHTML = multiCurrencyList[i].innerHTML.replace(moneyPattern, '<span class="multi-currency'+extraClass+'" usdprice="$1"></span>');
		}
	}

	function setupCurrencyValues(result) {
		var currencySymbol = '$';
		var currencyRate = 1.0;
		var cur = leh_var('cur');
		if (!cur) {
			cur = result.cur;
		}
		if (!cur) {
			cur = 'usd';
		} else {
			cur = cur.toLowerCase();
		}
		if (currencyList && currencyList.length > 0) {
			for (var i=0, n=currencyList.length; i<n; i++) {
				if (cur === currencyList[i].currency.toLowerCase()) {
					currencySymbol = currencyList[i].HTML_code;
					currencyRate = currencyList[i].exchange_rate;
					break;
				}
			}
		}

		updatePriceValues(currencyRate, currencySymbol, cur);
	}

	function setupImageShowCase() {
		var imageShowCaseList = document.getElementsByClassName('imageShowCase');
		for (var i = imageShowCaseList.length - 1; i >= 0; i--) {
			var showCaseImg = imageShowCaseList[i].querySelector('.showCase img');
			if (showCaseImg) {
				var thumbNailList = imageShowCaseList[i].querySelectorAll('.thumbNailList img');
				if (thumbNailList && thumbNailList.length > 0) {
					for (var j = thumbNailList.length - 1; j >= 0; j--) {
						thumbNailList[j].style.cursor = 'pointer';
						$(thumbNailList[j]).on('click', function() {
							showCaseImg.src = this.src;
							showCaseImg.srcset = this.srcset;
						})
					}
				}
			}
		}
	}

	function setupClickToCall(result) {
		if (result && result.actions && result.actions.checkout_url) {
			var url = result.actions.checkout_url+"";
			var match = /&PT=([0-9]+)/.exec(url);
			if (match) {
				var u = match[1];
				if (u) {
					var tapToCallLayer = document.getElementById('tapToCallLayer');
					if (tapToCallLayer) {
						$(tapToCallLayer).on('click', function() {
							$(tapToCallLayer).removeClass('view');
							setTimeout(function() {
								$(tapToCallLayer).removeClass('show');
							}, 1000);
						});

						var ttcPopup = tapToCallLayer.querySelector('.ttc-popup');
						if (ttcPopup) {
							var ttcAnchor = ttcPopup.querySelector('.ttc-button a');
							if (ttcAnchor) {
								var spacer = document.createElement('div');
								spacer.className = 'spacer';
								ttcPopup.parentNode.insertBefore(spacer.cloneNode(), ttcPopup);
								ttcPopup.parentNode.insertBefore(spacer, ttcPopup.nextSibling);

								var clickToCallList = document.getElementsByClassName('clickToCall');
								for (var i = clickToCallList.length - 1; i >= 0; i--) {
									var clickToCallAnchor = clickToCallList[i].querySelector('a');
									if (clickToCallAnchor && clickToCallAnchor.href.indexOf('tel:') === 0) {
										$(clickToCallAnchor).on('click', function() {
											ttcAnchor.href = clickToCallAnchor.href;
											$(tapToCallLayer).addClass('show');
											setTimeout(function() {
												$(tapToCallLayer).addClass('view');
											}, 50);

											return false;
										});
									}
								}
							}
						}
					}
				}
			}
		}
	}

	function setupPopupLinks() {
		var tve_editor = document.getElementById('tve_editor');
		var popupLinkList = document.querySelectorAll('.popupLink a');
		if (tve_editor && popupLinkList) {
			for (var i = popupLinkList.length - 1; i >= 0; i--) {
				var popupId = popupLinkList[i].href;
				var index = popupId.indexOf('#');
				if (index > -1) {
					popupId = popupId.substr(index + 1);
					var popupContainer = document.getElementById(popupId);
					if (popupContainer) {
						if (popupContainer.parentNode && popupContainer.parentNode.className == 'popupLayer') {
							var popupLayer = popupContainer.parentNode;
						} else {
							var popupLayer = document.createElement('div');
							popupLayer.className = 'popupLayer';
							popupLayer.style.backgroundColor = 'rgba(0,0,0,0.5)';
							popupLayer.style.width = '100%';
							popupLayer.style.height = '100vh';
							popupLayer.style.textAlign = 'center';
							popupLayer.style.zIndex = '15';
							popupLayer.style.cursor = 'pointer';

							tve_editor.appendChild(popupLayer);

							var spacer = document.createElement('div');
							spacer.className = 'spacer';
							popupLayer.appendChild(spacer.cloneNode(), popupLayer);
							popupLayer.appendChild(popupContainer);
							popupLayer.appendChild(spacer, popupLayer);
						}

						$(popupLinkList[i]).on('click', function() {
							$(popupLayer).addClass('show');
							setTimeout(function() {
								$(popupLayer).addClass('view');
							}, 50);

							return false;
						});

						$(popupLayer).on('click', function() {
							$(popupLayer).removeClass('view');
							setTimeout(function() {
								$(popupLayer).removeClass('show');
							}, 1000);
						});
					}
				}
			}
		}
	}

	$(function() {
		leh_get_cart(function(result) {
			if (result) {
				showPromoCode(result);
				if (result.total_units && result.line_items && result.line_items.length > 0) {
					setupCartIcon(result.line_items.length);
					createCart(result);
				} else {
					createEmptyCart();
				}
				showCouponInfo(result);
				setupMultiCurrency();
				setupCurrencyValues(result);
				setupProductBlock(result);
				setupClickToCall(result);
				setupPopupLinks();
				setupImageShowCase();
				showFreeShipping(result);
				translate();
				if (typeof showGDPR === "function") {
					showGDPR(result);
				}
				document.body.className += ' load-complete';
			} else {
				document.body.className += ' load-complete';
			}
		});
	});
})(jQuery);
