// Inline fp
var fpjs=function(e){"use strict";var t=function(){return(t=Object.assign||function(e){for(var t,R=1,r=arguments.length;R<r;R++)for(var n in t=arguments[R])Object.prototype.hasOwnProperty.call(t,n)&&(e[n]=t[n]);return e}).apply(this,arguments)};function R(e){for(var t="",R=0;R<e.length;++R)if(R>0){var r=e[R].toLowerCase();r!==e[R]?t+=" "+r:t+=e[R]}else t+=e[R].toUpperCase();return t}var r="Client timeout",n="Network connection error",o="Network request aborted",E="Response cannot be parsed",i="Blocked by CSP",O=R("WrongRegion"),_=R("SubscriptionNotActive"),a=R("UnsupportedVersion"),u=R("InstallationMethodRestricted"),c=R("HostnameRestricted"),l="API key required",s="API key not found",I="API key expired",d="Request cannot be parsed",N="Request failed",f="Request failed to process",T="Too many requests, rate limit exceeded",p="Not available for this origin",v="Not available with restricted header",A=l,D=s,P=I,h="3.7.1";function S(e,t,R){return void 0===R&&(R=0),t(R).catch((function(r){if(R>=e.maxRetries||!e.shouldRetry(r))throw r;var n,o,E,i,O=(n=e.baseDelay,o=e.maxDelay,E=e.baseDelay*Math.pow(2,R),Math.max(n,Math.min(o,E)));return(i=O,new Promise((function(e){return setTimeout(e,i)}))).then((function(){return S(e,t,R+1)}))}))}var y="Failed to load the JS script of the agent",m="https://fpnpmcdn.net/v<version>/<apiKey>/loader_v<loaderVersion>.js";function w(e){var R,r,n=e.scriptUrlPattern,o=e.token,E=e.apiKey,O=void 0===E?o:E,_=function(e,t){var R={};for(var r in e)Object.prototype.hasOwnProperty.call(e,r)&&t.indexOf(r)<0&&(R[r]=e[r]);if(null!=e&&"function"==typeof Object.getOwnPropertySymbols){var n=0;for(r=Object.getOwnPropertySymbols(e);n<r.length;n++)t.indexOf(r[n])<0&&Object.prototype.propertyIsEnumerable.call(e,r[n])&&(R[r[n]]=e[r[n]])}return R}(e,["scriptUrlPattern","token","apiKey"]),a=(R=e,r="scriptUrlPattern",Object.prototype.hasOwnProperty.call(R,r)?n:void 0),u=[];return Promise.resolve().then((function(){if(!O||"string"!=typeof O)throw new Error(l);return S({maxRetries:5,baseDelay:100,maxDelay:3e3,shouldRetry:function(e){return!(e instanceof Error&&e.message===i)}},(function(){var e,t=new Date,R=function(){return u.push({startedAt:t,finishedAt:new Date})},r=function(e,t,R,r){var n,o=document,E="securitypolicyviolation",i=function(t){var R=new URL(e,location.href),r=t.blockedURI;r!==R.href&&r!==R.protocol.slice(0,-1)&&r!==R.origin||(n=t,O())};o.addEventListener(E,i);var O=function(){return o.removeEventListener(E,i)};return null==r||r.then(O,O),Promise.resolve().then(t).then((function(e){return O(),e}),(function(e){return new Promise((function(e){return setTimeout(e)})).then((function(){if(O(),n)return R(n);throw e}))}))}(e=function(e,t){void 0===t&&(t=m);var R=encodeURIComponent;return t.replace(/<[^<>]+>/g,(function(t){return"<version>"===t?"3":"<apiKey>"===t?R(e):"<loaderVersion>"===t?R(h):t}))}(O,a),(function(){return function(e){return new Promise((function(t,R){var r=document.createElement("script"),n=function(){var e;return null===(e=r.parentNode)||void 0===e?void 0:e.removeChild(r)},o=document.head||document.getElementsByTagName("head")[0];r.onload=function(){n(),t()},r.onerror=function(){n(),R(new Error(y))},r.async=!0,r.src=e,o.appendChild(r)}))}(e)}),(function(){throw new Error(i)}));return r.then(R,R),r}))})).then((function(){var e=window,R="__fpjs_p_l_b",r=e[R];if(function(e,t){var R,r=null===(R=Object.getOwnPropertyDescriptor)||void 0===R?void 0:R.call(Object,e,t);(null==r?void 0:r.configurable)?delete e[t]:r&&!r.writable||(e[t]=void 0)}(e,R),"function"!=typeof(null==r?void 0:r.load))throw new Error(y);return r.load(t(t({},_),{ldi:{attempts:u}}))}))}var b={load:w,ERROR_SCRIPT_LOAD_FAIL:y,ERROR_API_KEY_EXPIRED:I,ERROR_API_KEY_INVALID:s,ERROR_API_KEY_MISSING:l,ERROR_BAD_REQUEST_FORMAT:d,ERROR_BAD_RESPONSE_FORMAT:E,ERROR_CLIENT_TIMEOUT:r,ERROR_CSP_BLOCK:i,ERROR_FORBIDDEN_ENDPOINT:c,ERROR_FORBIDDEN_HEADER:v,ERROR_FORBIDDEN_ORIGIN:p,ERROR_GENERAL_SERVER_FAILURE:N,ERROR_INSTALLATION_METHOD_RESTRICTED:u,ERROR_NETWORK_ABORT:o,ERROR_NETWORK_CONNECTION:n,ERROR_RATE_LIMIT:T,ERROR_SERVER_TIMEOUT:f,ERROR_SUBSCRIPTION_NOT_ACTIVE:_,ERROR_TOKEN_EXPIRED:P,ERROR_TOKEN_INVALID:D,ERROR_TOKEN_MISSING:A,ERROR_UNSUPPORTED_VERSION:a,ERROR_WRONG_REGION:O};return e.ERROR_API_KEY_EXPIRED=I,e.ERROR_API_KEY_INVALID=s,e.ERROR_API_KEY_MISSING=l,e.ERROR_BAD_REQUEST_FORMAT=d,e.ERROR_BAD_RESPONSE_FORMAT=E,e.ERROR_CLIENT_TIMEOUT=r,e.ERROR_CSP_BLOCK=i,e.ERROR_FORBIDDEN_ENDPOINT=c,e.ERROR_FORBIDDEN_HEADER=v,e.ERROR_FORBIDDEN_ORIGIN=p,e.ERROR_GENERAL_SERVER_FAILURE=N,e.ERROR_INSTALLATION_METHOD_RESTRICTED=u,e.ERROR_NETWORK_ABORT=o,e.ERROR_NETWORK_CONNECTION=n,e.ERROR_RATE_LIMIT=T,e.ERROR_SCRIPT_LOAD_FAIL=y,e.ERROR_SERVER_TIMEOUT=f,e.ERROR_SUBSCRIPTION_NOT_ACTIVE=_,e.ERROR_TOKEN_EXPIRED=P,e.ERROR_TOKEN_INVALID=D,e.ERROR_TOKEN_MISSING=A,e.ERROR_UNSUPPORTED_VERSION=a,e.ERROR_WRONG_REGION=O,e.default=b,e.load=w,Object.defineProperty(e,"__esModule",{value:!0}),e}({});

// Default the cookie expiration at 5 years
const FIVE_YEARS_IN_DAYS = 365*5;

function orderBridge(e) {
  var link        = new URL(e.href);
  var searchParams= new URLSearchParams(link.search);
  let params      = '';
  let targetlink  = '/checkout/?oid=' + link.pathname.replaceAll('/', '');

  if (searchParams) {
    for(var [key, value] of searchParams.entries()) {
      params += '&' + key + '=' + value;
    };

    targetlink += params;
  }

  window.location.href = targetlink;

  return '';
}

/**
 * Get the variable from the cookie
 *
 * @param string varname without the
 * @param {*} def Not sure what this is used for
 *
 * @return mixed cookie value or null
 */
function leh_var(varname, def) {
  varname = 'leh_' + varname;
  if (Cookies.get(varname)) {
    return Cookies.get(varname);
  }
  if (typeof (def) !== "undefined") {
    return def;
  }
  return null;
}

/**
 * Set the cookie variable
 *
 * @param string varname Variable Name
 * @param {*} value
 *
 * @return void
 */
function leh_set(varname, value) {
  varname = 'leh_' + varname;
  Cookies.set(varname, value.toLowerCase(), { expires: FIVE_YEARS_IN_DAYS, secure: true });
}

/**
 * Gets the list of currencies
 *
 * @return list of currencies
 */
function leh_get_currency_list() {
  return JSON.parse(decodeURIComponent(leh_var('currencies')));
}

/**
 * Validates the email address calling to the restfull api
 *
 * @param {*} callback Calls callback function to return the JSON
 *
 */
function leh_validate_email(email_address, callback) {
  leh_api_request('/address/validate/' + email_address, 'GET', callback);
}

/**
 * An ajax post to update the quantity
 *
 * @param {*} select The dom element on change for the select list
 * @param {*} callback Calls callback function to return the JSON
 *
 * @return void
 */
function leh_update_quantity(cart_ref, qty, callback) {
  leh_request('/v2/cart/update/' + cart_ref + '/' + qty, 'POST', callback);
}

/**
 * Calls off to the ajax function to remove the cart item
 *
 * @param {*} cart_ref
 * @param {*} callback Calls callback function to return the JSON
 *
 */
function leh_remove_product(cart_ref, callback) {
  leh_request('/v2/cart/remove/' + cart_ref, 'DELETE', callback);
}

/**
 * An ajax post to add an item
 *
 * @param {*} cart_ref The item reference for the cart
 * @param {*} qty      The Quantity to add
 * @param {*} callback Calls callback function to return the JSON
 *
 * @return void
 */
function leh_add_to_cart(cart_ref, qty, callback) {
  leh_api_request('/cart/add/' + cart_ref + '/' + qty, 'POST', callback);
}

/**
 * Calls off to the ajax function to remove the cart item
 *
 * @param {*} callback Calls callback function to return the JSON
 *
 */
function leh_remove_coupon(callback) {
  leh_api_request('/cart/cpn/remove', 'DELETE', function() {
    leh_api_request('/cart', 'GET', callback);
  });
}

/**
 * Calls off to the ajax function to get the current cart json
 *
 * @param {*} callback Calls callback function to return the JSON
 *
 */
function leh_get_cart(callback) {
  leh_request('/v2/promo/cart', 'GET', callback);
}

/**
 * Calls off to the ajax function to do api operations
 *
 * @param {*} params   Operation parameters to add to the request URL
 * @param {*} type     Request type
 * @param {*} callback Calls callback function to return the JSON
 *
 */
function leh_api_request(params, type, callback) {
  jQuery.ajax({
    url: '/wp-json/wordplug/v1' + params,
    type: type,
    success: function (result) {
      callback((result));
    },
    fail: function (result) {
      callback({fail:true, result:result});
    }
  });
}

/**
 * POST an array of English phrases to have translated to the language
 * associated with the passed Country Code.
 *
 * @param {*} phrases   Array of phrases to translate
 * @param {*} cc        Two letter country code to translate to
 * @param {*} callback  Calls callback function to return the JSON
 *
 */
function leh_translate(phrases, cc, callback) {
  jQuery.ajax({
    url: '/wp-json/wordplug/v2/translate',
    type: 'POST',
    data: {
      lang: cc,
      phrases: phrases,
    },
    success: function (result) {
      callback(result);
    },
    fail: function (result) {
      callback({fail:true, result:result});
    }
  });
}

/**
 * Calls off to the ajax function to do api operations against the
 * v2 api.
 *
 * @param {*} params   Operation parameters to add to the request URL
 * @param {*} type     Request type
 * @param {*} callback Calls callback function to return the JSON

 */
 function leh_request(params, type, callback) {
    if (params.substring(0, 1) != '/') {
      params = '/' + params;
    }

    jQuery.ajax({
      url: '/wp-json/wordplug' + params,
      type: type,
      success: function (result) {
        callback(result);
      },
      fail: function (result) {
        callback({fail:true, result:result});
      }
    });
  }

/**
 * Check a remote site for a uid.
 *
 * @param {*} fp        Unique user print
 * @param {*} host      Host URL to check against (source-website)
 * @param {*} callback  Calls callback function to return the JSON
 */
 function leh_hostCheck(fp, host, callback) {
    jQuery.ajax({
      url: '/wp-json/wordplug/v2/usearch?fp=' + fp + '&ws=' + host,
      type: 'GET',
      success: function (result) {
          callback(result);
      },
      error: function (result) {
          callback({
            fail: true,
            result: null
          });
      }
    });
  }

/**
 * Set the print for this user.
 *
 * @param {*} fp        Unique user print
 */
 function leh_print(fp) {
  jQuery.ajax({
    url: '/wp-json/wordplug/v2/ping?p=' + fp,
    type: 'GET',
  });
}

// Check for leh_p, if missing call fp service, first via subdomain,
// otherwise fallback to the CDN method.
if (!leh_var('p') || (leh_var('p') && leh_var('p').length <= 10)) {
  var subdomain = 'https://fp.' + window.location.hostname.split('.').slice(1).join('.');
  
  const fpPromise = fpjs.load({
    apiKey: 'ZbldbZun5j0bDdHfOiUi',
    endpoint: subdomain
  });

  (async () => {
    try {
      const fp = await fpPromise;
      const result = await fp.get({linkedId: window.location.host});
      leh_set('p', result.visitorId);
      leh_print(result.visitorId);
    } catch {
      // Fail silently. Attempt via CDN.
      const fpCdnPromise = import('https://fpcdn.io/v3/ZbldbZun5j0bDdHfOiUi').then(FingerprintJS => FingerprintJS.load());
      fpCdnPromise
        .then(fp => fp.get({linkedId: window.location.host}))
        .then(function (result) {
          leh_set('p', result.visitorId);
          leh_print(result.visitorId);
        });
    }
  })();
}

let match = window.location.href.match(/\?(\d+)/)
if (match) {
  let targetUid = match[1];
  document.addEventListener('DOMContentLoaded', function() {
    if (jQuery('#source-website').length) {
      let sourceSite = document.querySelector('[data-website]').dataset['website'];
      var promise = new Promise(function(resolve) {

        // We have a product source and print. Check for a match with the source site.
        leh_hostCheck(fingerprint, sourceSite, function(result) {
          if (result) {
            if (result.uid) {
              targetUid = result.uid;
            }

            resolve(targetUid);
          }
        });
      });

      promise.then(function(res){
        if (!leh_first_click || leh_var('u') == null) {
          if (res) {
            updateUid(res);
          }
        }
      });

    } else {
      if (!leh_first_click || leh_var('u') == null) {
        updateUid(targetUid);
      }
    }
  });
} else if(""+document.referrer !== "" && !Cookies.get("leh_u")) {
    // if the refferrer is blank do not update the referrer
    // if there is a ct user then do not update the referrer
    leh_api_request('/referrer/' + encodeURI(document.referrer), 'GET', function(result){
        if(result && result.ref) {
            Cookies.set('leh_ref', result.ref, {expires: FIVE_YEARS_IN_DAYS, secure: true});
        }
    });
}

function updateUid(targetUid) {
  Cookies.set('leh_u', targetUid);
  Cookies.remove('leh_ref');
  Cookies.remove('leh_t');
  Cookies.remove('leh_t1');
  Cookies.remove('leh_t2');
  Cookies.remove('leh_t3');
  Cookies.remove('leh_t4');
  Cookies.remove('leh_t5');
  Cookies.remove('leh_b');
  Cookies.remove('leh_c');
  Cookies.remove('leh_a');
  Cookies.remove('leh_d');
  Cookies.remove('leh_r');
  Cookies.remove('leh_src');
  Cookies.remove('leh_lid');
  Cookies.remove('leh_pool_id');
  Cookies.remove('leh_cpu');
  Cookies.remove('leh_ncr');
  Cookies.remove('leh_acv');
  Cookies.remove('leh_ct');

  let rap = leh_var('rap');

  // Add leh_u var to the top of an rap existing array. Shift others down.
  if (rap) {
    rap = rap.split(':');
    rap.unshift(targetUid);
    if (rap.length > 5) {
      rap.length = 5;
    }
    rap = rap.join(':');

    // Set the newly formed array back into leh_rap.
    leh_set('rap', rap);
  } else {
    // If no rap is present, add the singular value.
    leh_set('rap', targetUid);
  }

}

function getScript(source, callback) {
  var script = document.createElement('script');
  var prior = document.getElementsByTagName('script')[0];
  script.async = 1;

  script.onload = script.onreadystatechange = function( _, isAbort ) {
      if(isAbort || !script.readyState || /loaded|complete/.test(script.readyState) ) {
          script.onload = script.onreadystatechange = null;
          script = undefined;

          if(!isAbort && callback) setTimeout(callback, 0);
      }
  };

  script.src = source;
  prior.parentNode.insertBefore(script, prior);
}

// If a uId exists, search and render any pixels.
if (leh_var('u')) {
  fetch('/wp-json/wordplug/v2/pixel')
  .then(
    response => response.json()
  )
  .then(
    function (data) {
      data.trackers.forEach(tracker => {
        if (tracker.script) {
          let networkScript = document.createElement('script');
          networkScript.setAttribute('src', tracker.script);
          networkScript.setAttribute('data-type', tracker.network);
          document.body.appendChild(networkScript);
        }
        if (tracker.payload) {
          eval(tracker.payload);
        }
      });
    }
  );
}
