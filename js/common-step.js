function gen4() {
    this.init          = init;
    this.setTMPL       = setTMPL;
    this.advance       = advance;
    this.stepJump      = stepJump;
    this.saveShipping  = saveShipping;
    this.saveInsurance = saveInsurance;
    this.saveCoupon    = saveCoupon;
    this.removeCoupon  = removeCoupon;
    this.processPPO    = processPPO;
    this.processUpsell = processUpsell;
    this.info          = getTMPL;
    this.getSbt         = getSbt;
    this.setAdvNav      = setSbt;
    this.showCoupon     = showCoupon;
    this.getSubmitLock = getSubmitLock;
    this.setSubmitLock = setSubmitLock;
    this.submitBlock = submitBlock;
    this.fnl_yes = funnel_accept;
    this.fnl_no = funnel_decline;
    this.fnl_preview_yes = funnel_preview_accept;
    this.fnl_preview_no = funnel_preview_decline;
    this.calcTaxes  = calcTaxes;
    this.taxValidateChoice  = taxValidateChoice;
    this.doCartLoad = _loadCart;
    this.saveLanguage=saveLanguage;
    this.initCheckout=initCheckout;
    this.vh = _validatorHeartbeat;
  
    self=this;
    const maxRetries = 15;
    const retryDelay = 5000;
    const funnelRetryDelay = 2000;
    let TMPL  = {};
    let allPanelsOK = false;
    let targetCoupon = '';
    let ppoPresented = false;
    let currentAttempts = 0;
    let sbt = false;
    let whichUpsell = '';
    let validationNeeded = true;
    let validatedAddress = [];
    let submitLock = false;
    let grabbedModals = false;
    let ccMode = 'nmi';
    let checkoutValidated = false;
    let checkoutCardValid = false;
    let checkoutEXPValid = false;
    let checkoutCVVValid = false;
    let sameAsShippingToggled = false;
  
    function getTMPL(){
      return [TMPL['SELF'], TMPL['OID'], TMPL['SESSION'], TMPL['LANG']];
    }
  
    function setTMPL(a,b,c,d,e,f,g,h,i,j) {
      TMPL['SELF'] = a;
      TMPL['OID'] = b;
      TMPL['SESSION'] = c;
      TMPL['NEXTSTEP'] = d;
      TMPL['ORDERBLOCK'] = e;
      TMPL['NMI_ATTEMPTS'] = parseInt(f);
      TMPL['NMISESSION'] = g;
      TMPL['PPSESSION'] = h;
      TMPL['PAYMENT_METHOD'] = i;
      if (j) {
        TMPL['LANG'] = j;
      } else {
        TMPL['LANG'] = 'en';
      }
    }
  
    function init(mode) {
      ///// Special exist for funnel preview
      if (mode == 'fnl_preview') {
        return;
      }
  
      ///// This forces a reload when someone uses the "BACK" button. Without it Safari browsers
      ///// return to previous order steps where the Submit Block state was left blocked, preventing
      ///// moving forward.
      $(window).bind("pageshow", function(event) {
        if (event.originalEvent.persisted) {
          window.location.reload();
        }
      });
      $('#modal-coupon-code').modal({
        // starting_top: '8%',
        // ending_top: '50%',
        ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
        }
      });
  
      if (mode == 'proc') {
        if ($('#pp_token').val() != null) {
          if ($('#pp_token').val() != "") {
            $('#modal1').modal('open');
          }
        }
  
        $('#modal1').modal({
          dismissible: false,
          opacity: .5,
          in_duration: 300,
          out_duration: 200,
          starting_top: '8%',
          ending_top: '50%',
          ready: function(modal, trigger) {
            _init_common();
            _evalOrderBlock();
            _loadCart();
          },
          complete: function() { opacity: 0; }
        });
  
        setTimeout(function() {_checkProcessingStatus();},retryDelay);
      } else if (mode == 'funnel') {
        $('.modal').modal();
        $('#process_modal').modal({
          dismissible: false,
          opacity: .5,
          in_duration: 300,
          out_duration: 200,
          starting_top: '8%',
          ending_top: '50%',
          ready: function(modal, trigger) {},
          complete: function() {}
        });
      } else if (mode == 'easyship') {
        _init_common();
        _init_panels();
        _evalOrderBlock();
        _evalPayMethod();
        gEShipRates();
      } else {
        _init_common();
        _init_panels();
        _evalOrderBlock();
        _evalPayMethod();
        _loadCart();
      };
      if ($('#email').length) {
        _submitBrowserData();
      }
      if ($('#billing-cc-number').length) {
        $('#billing-cc-number').validateCreditCard(function(result) {
          if ($('#billing-cc-number').val().length > 3) {
            $('.payment-icon').css('opacity', '0.2');
            if (result.card_type) {
              $('.payment-icon-cc-' + result.card_type.name).css('opacity', '1');
              var $cc = $('input[name=billing-cc-number]');
              $cc.rules('remove', 'cc-mc');$cc.rules('remove', 'cc-visa');$cc.rules('remove', 'cc-mcevm');
              $cc.rules('remove', 'cc-disc');$cc.rules('remove', 'cc-amex');$cc.rules('remove', 'max');
              switch(result.card_type.name) {
                case 'visa':
                  $cc.rules('add', 'cc-visa');
                  break;
                case 'mastercard':
                  $cc.rules('add', 'cc-mc');
                  break;
                case 'mastercard-emv':
                  $cc.rules('add', 'cc-mcevm');
                  break;
                case 'amex':
                  $cc.rules('add', 'cc-amex');
                  break;
                case 'discover':
                  $cc.rules('add', 'cc-disc');
                  break;
              }
              if (($('.payment-icon-cc-discover').length == 0 && result.card_type.name == 'discover') || ($('.payment-icon-cc-amex').length == 0 && result.card_type.name == 'amex') ) {
                M.toast( {html: '<p>'+_translate('You are entering an unsupported credit card type!')+'</p>'} );
                $cc.rules('remove', 'cc-disc');$cc.rules('remove', 'cc-amex');
                // Ensure invalidation...
                $cc.rules('add', {max:0});
              }
            }
          } else {
            $('.payment-icon').css('opacity', '1');
          }
        });
      }
      if (typeof gen4_localize !== "function") {
        ///// If the language translation matrix wasn't loaded, we need to make sure language assets
        ///// for the correct root language are made visible. If the matrix was loaded, this will be
        ///// taken care of automatically.
        $('.gen4lang_assets').hide();
        $('.gen4lang_asset_'+$('html')[0].lang).show();
      }
    }
  
    function _init_common() {
  
      /* Init actions common to all steps */
      $(window).keydown(function(event){
        if (event.keyCode == 13) {
          if (event.target.id.indexOf('mobile') !== -1) {
            g4.saveCoupon('mobile');
          } else {
            g4.saveCoupon('desktop');
          }
          event.preventDefault();
          return false;
        }
      });
  
      $(document).on('click', function() {
  
        // $('select').material_select();
        // $('select').formSelect();
  
      });
  
      $('.coupon-list').click(function(el) {
        $('#discount-card, #discount-card-mobile').val(el.target.textContent);
        $('#discount-card, #discount-card-mobile').focus();
        $('#apply-coupon').trigger('click');
        $('#modal-coupon-code').modal('close');
      });
  
      $('#apply-coupon').click($.debounce(function() {
        g4.saveCoupon('desktop');
      }, 500));
      $('#apply-coupon-mobile').click($.debounce(function() {
        g4.saveCoupon('mobile');
      }, 500));
  
      $('.modal').modal();
      $('#privacy-policy, #mutual-arbitration-agreement, #terms-and-conditions, #money-back-guarantee').modal({
        onOpenStart: function() {
          if (!grabbedModals) {
            _getModals();
          } else {
            $('.gen4lang_assets').hide();
            var lang = $('html')[0].lang;
            if ($('#choose_language').length) {
              lang = $('#choose_language').val();
            }
            $('.gen4lang_asset_'+lang).show();
          }
        },
      });
      if ($('#payment-collapsible').length) {
        _togglePayment();
      }
  
      $("input[type=text],input[type=email],input[type=tel]").blur(function() { $(this).val($(this).val().replace(/^\s+|\s+$/g,"")); });
  
      if ($('#process_modal').length) {
        $('#process_modal').modal({
          dismissible: false,
          opacity: .5,
          in_duration: 300,
          out_duration: 200,
          starting_top: '8%',
          ending_top: '50%',
          ready: function(modal, trigger) {},
          complete: function() {}
        });
      }
  
      $('#av_modal').modal({
        dismissible: false,
        opacity: .5,
        in_duration: 300,
        out_duration: 200,
        starting_top: '8%',
        ending_top: '50%',
        ready: function(modal, trigger) {},
        complete: function() {}
      });
  
      $('#av_modal_err').modal({
        dismissible: false,
        opacity: .5,
        in_duration: 300,
        out_duration: 200,
        starting_top: '8%',
        ending_top: '50%',
        ready: function(modal, trigger) {},
        complete: function() {}
      });
  
      if ($('#ppo-offer').length) {
        $('#ppo-offer').modal({
          dismissible: false,
          opacity:     0.85,
          starting_top: '4%',
          ending_top: '10%',
          ready: function(modal, trigger) {},
          complete: function() {}
        });
      }
  
      if ($('#alert_modal').length) {
        $('#alert_modal').modal({
          dismissible: false,
          opacity: .5,
          in_duration: 300,
          out_duration: 200,
          starting_top: '8%',
          ending_top: '50%',
          ready: function(modal, trigger) {},
          complete: function() {}
        });
      }
  
      $('.collapsible').collapsible();
  
    }
  
    function _getModals() {
      $('#privacy-policy .modal-content').html('');
      $('#mutual-arbitration-agreement .modal-content').html('');
      $('#terms-and-conditions .modal-content').html('');
      $('#money-back-guarantee .modal-content').html('');
      var nocache = new Date();
      $.getJSON(TMPL['SELF'] + "?rm=getModals&session="+TMPL['SESSION']+"&nocache=" + nocache.getTime(), function(data) {
        if (data['privacy']) { $('#privacy-policy .modal-content').html(data['privacy']); }
        if (data['arbitration']) { $('#mutual-arbitration-agreement .modal-content').html(data['arbitration']); }
        if (data['terms']) { $('#terms-and-conditions .modal-content').html(data['terms']); }
        if (data['guarantee']) { $('#money-back-guarantee .modal-content').html(data['guarantee']); }
        $('.gen4lang_assets').hide();
        var lang = $('html')[0].lang;
        if ($('#choose_language').length) {
          lang = $('#choose_language').val();
        }
        $('.gen4lang_asset_'+lang).show();
        grabbedModals = true;
      }).fail(() => {
        _validatorHeartbeat('commerror-saveData');
        g4.setSubmitLock(false);
        $('#alert-content-modal').html("<p>"+_translate('You have lost connection to the order server. Please check your connection and refresh this page.')+"</p>");
        $('#alert_modal').modal('open');
      });
    }
  
    function _init_panels() {
      /* Init actions when specific panels are present only */
  
      // Ensure cssmaterialized selectboxes are being validated on change properly...
      var selectBoxes = ['#ship_salutation', '#bill_salutation', '#ship_prov', '#ship_state', '#ship_aus', '#bill_aus', '#bill_prov', '#bill_state', '#billing_expiry_month', '#billing_expiry_year'];
      for (var i = 0, len = selectBoxes.length; i < len; i++) {
        $(selectBoxes[i]).on('change', function(ele) {
          $(this).valid();
  
          // $('select').material_select();
          // $('select').formSelect();
  
        });
      }
  
      /* ======== CREDIT CARD FORM ========= */
  
      // Force radio selection on header-area click...
      $('.collapsible-header.credit, .collapsible-header.paypal, .collapsible-header.sofort').on('click', function(event){
        $(this).children('input[type=radio]').prop('checked', true);
        _togglePayment();
      });
  
      // Handle panel with multiple radios...
      $('.collapsible-header.other').on('change', function(event){
        $('ul#payment-collapsible .credit input[type=radio], ul#payment-collapsible .paypal input[type=radio], ul#payment-collapsible .sofort input[type=radio]').prop('checked', false);
        _togglePayment();
      });
  
  
      $('.collapsible-header.bill-same, .collapsible-header.bill-diff').on('click', function(event){
        $(this).children('input[type=radio]').prop('checked', true);
        _selectCountry('bill');
        _toggleBilling(this);
      });
  
      /* ======== SHIPPING ADDRESS ========= */
      if ($('#ship_country').length) {
        _selectCountry('ship');
        $('#ship_country').on('change', function(){
          _selectCountry('ship',true);
        });
      };
  
      /* ======== BILLING ADDRESS ========= */
      if ($('#bill_country').length) {
        _selectCountry('bill');
        $('#bill_country').on('change', function(){
          _selectCountry('bill',true);
        });
      };
  
      _toggleBilling('init');
  
      /* ======== SHIPPING METHODS ========= */
      $('input[name="ship_method_id"]').click($.debounce(function() {
        saveShipping();
      }, 500));
  
      $('input[name="accepted_ship_insurance"]').click($.debounce(function() {
        saveInsurance();
      }, 500));
  
      $('input[name="sms_consent"]').click($.debounce(function() {
        saveSMS();
      }, 500));
  
      $('input[name="gdprconsent"]').click($.debounce(function() {
        saveGDPR();
      }, 500));
  
      $('.accepted_upsell_inline').each(function(i,v) {
        var upid = $(v).attr('upid');
        $('#accepted_upsell_inline_'+upid).click($.debounce(function() {
          processUpsell(upid);
        }, 500));
      });
  
      if ($('#pp_token').length && $('#pp_token').val() != "") {
        var nocache = new Date();
        var postParams = { rm: 'wait', oid: TMPL['OID'], session: TMPL['SESSION'], nocache: nocache.getTime() };
  
        $.post(TMPL['SELF'], postParams, function(data) {
          if (data['response'] == 'success') {
            $('#modal1').modal('close');
          }
        }, 'json');
      }
  
    }
  
    function advance() {
      if (allPanelsOK) {
        if (TMPL['NEXTSTEP'] == 'process') {
          if ($('#ppo').length && !ppoPresented) {
                      var nocache = new Date();
                      $.get(TMPL['SELF']+"?rm=checkppo&oid="+TMPL['OID']+"&session="+TMPL['SESSION']+"&upsellID="+$('#PPOID').val()+"&nocache="+nocache.getTime(), function(data){
                          if (data == 'success') {
                    ppoPresented = true;
                    _validatorHeartbeat('ppo');
                    $('#ppo-offer').modal('open');
                          } else {
                              _validatorHeartbeat('ppo-blocked');
                    _processOrder();
                          }
                      }).fail(() => {
              _validatorHeartbeat('commerror-checkppo');
              _processOrder();
            });
          } else {
            if (ccMode=='checkout' && !checkoutValidated && $('#credit').is(':checked')) {
              _validatorHeartbeat('checkout');
              if (!checkoutCardValid) {
                M.toast( {html: '<p>'+_translate('Your credit card number is missing or invalid.')+'</p>'} );
              } else if (!checkoutEXPValid) {
                M.toast( {html: '<p>'+_translate('Your credit card expiry is missing or invalid.')+'</p>'} );
              } else if (!checkoutCVVValid) {
                M.toast( {html: '<p>'+_translate('Your credit card security code is missing or invalid.')+'</p>'} );
              }
              g4.setSubmitLock(false);
            } else {
              _processOrder();
            }
          }
        } else {
          sbt = true;
          stepJump(TMPL['NEXTSTEP']);
        }
      } else {
        _checkPanels(function(){advance()});
      }
    }
  
    function getSbt() {
      return sbt;
    }
  
    function setSbt(sbt_setting) {
      if (sbt_setting != null) {
        sbt = sbt_setting;
      }
    }
  
    function _saveData(w, callback) {
      var postParams = {};
      switch (w) {
        case 'bill':
          if ($('#same').is(':checked')) {
            postParams['makeSame'] = 'yes';
          } else {
            postParams['makeSame'] = 'no';
          }
        case 'ship':
          postParams['panel'] = w+'-address';
          if ($('#'+w+'_salutation').val() != null && $('#'+w+'_salutation').val() != "") {
            postParams[w+'_salutation'] = $('#'+w+'_salutation').val();
          }
          postParams[w+'_fname']      = $('#'+w+'_fname').val();
          postParams[w+'_lname']      = $('#'+w+'_lname').val();
          postParams[w+'_addr1']      = $('#'+w+'_address').val();
          postParams[w+'_addr2']      = $('#'+w+'_address_cont').val();
          postParams[w+'_city']       = $('#'+w+'_city').val();
          postParams[w+'_country']    = $('#'+w+'_country').val();
          postParams[w+'_phone']      = $('#'+w+'_phone').val();
          if ($('#'+w+'_country').val() == 'us' || $('#'+w+'_country').val() == 'mil') {
            postParams[w+'_state']    = $('#'+w+'_state').val();
            postParams[w+'_zip']      = $('#'+w+'_zip').val();
  
          } else if ($('#'+w+'_country').val() == 'ca') {
            postParams[w+'_prov']     = $('#'+w+'_prov').val();
            postParams[w+'_post_ca']  = $('#'+w+'_post').val();
          } else if ($('#'+w+'_country').val() == 'au') {
            postParams[w+'_auregion']     = $('#'+w+'_aus').val();
            postParams[w+'_post_au']  = $('#'+w+'_auspost').val();
          } else {
            if ($('#'+w+'_other').val() != null) {
              postParams[w+'_region']     = $('#'+w+'_other').val();
            }
            postParams[w+'_post_other'] = $('#'+w+'_post_other').val();
          }
          if ($('#sms_consent').length) {
            if ($('#sms_consent').prop("checked") == true) {
              postParams['sms_consent'] = 'y';
            } else {
              postParams['sms_consent'] = 'n';
            }
          }
          break;
        case 'email':
          postParams['panel'] = 'email';
          postParams['email'] = $('#email').val();
          break;
        case 'tvOverride':
          postParams['panel'] = 'tvOverride';
          break;
        case 'ship-method':
          postParams['panel']   = 'ship-method';
          postParams['ship_method_id'] = document.querySelector('input[name="ship_method_id"]:checked').value;
          postParams['courierid'] = document.querySelector('input[name="ship_method_id"]:checked').getAttribute('courierid');
          $('#proc_content').html('<p>'+_translate('Saving Shipping. Please wait...')+'</p>');
          $('#process_modal').modal('open');
          break;
        case 'insurance':
          postParams['panel']  = 'insurance';
          if ($('#accepted_ship_insurance').prop("checked") == true) {
            postParams['newVal'] = 'y';
          } else {
            postParams['newVal'] = 'n';
          }
          $('#proc_content').html('<p>'+_translate('Saving Shipping Insurance. Please wait...')+'</p>');
          $('#process_modal').modal('open');
          break;
        case 'sms_consent':
          postParams['panel']  = 'sms_consent';
          if ($('#sms_consent').prop("checked") == true) {
            postParams['newVal'] = 'y';
          } else {
            postParams['newVal'] = 'n';
          }
          break;
        case 'gdpr':
          postParams['panel']  = 'gdpr';
          if ($('#gdprconsent').prop("checked") == true) {
            postParams['newVal'] = 'y';
          } else {
            postParams['newVal'] = 'n';
          }
          break;
        case 'language':
          postParams['panel']  = 'language';
          postParams['newVal'] = $('#choose_language').val();
          break;
        case 'coupon':
          if (targetCoupon != '') {
            postParams['panel']  = 'coupon';
            postParams['action'] = 'add';
            postParams['coupon'] = targetCoupon;
          }
          break;
        case 'coupon-r':
          postParams['panel']  = 'coupon';
          postParams['action'] = 'remove';
          break;
        case 'ppo':
          postParams['panel']  = 'ppo';
          postParams['ppoID'] = $('#PPOID').val();
          break;
        case 'upsell':
          if (whichUpsell != '') {
            postParams['panel']  = 'upsell';
            postParams['action'] = 'add';
            postParams['upsellID'] = whichUpsell;
          }
          $('#proc_content').html('<p>'+_translate('Adding Item. Please wait...')+'</p>');
          $('#process_modal').modal('open');
          break;
        case 'upsell-r':
          if (whichUpsell != '') {
            postParams['panel']  = 'upsell';
            postParams['action'] = 'remove';
            postParams['upsellID'] = whichUpsell;
          }
          break;
        case 'ship-country':
          postParams['panel']  = 'ship-country';
          postParams['ccode']  = $('#ship_country').val();
          break;
      };
  
      if ('panel' in postParams) {
        var nocache = new Date();
        postParams['rm']      = 'su';
        postParams['oid']     = TMPL['OID'];
        postParams['session'] = TMPL['SESSION'];
        postParams['nocache'] = nocache.getTime();
  
  
        $.post(TMPL['SELF'], postParams, function(data) {
          if (data['response'] == 'success') {
            if (w == 'ship-country' && data['extras']['coupon-reset']) {
                      targetCoupon = data['extras']['coupon-reset'];
                      _saveData('coupon', function() { callback(); });
            } else {
                if (w == 'coupon' && data['extras']['status'] == 'BAD') {
               M.toast({html: '<p>' + _translate(data['extras']['msg']) + '</p>'});
  
                  $('#discount-card, #discount-card-mobile').val(null);
                  $('#discount-card, #discount-card-mobile').blur();
                }
  
                /* If something about shipping methods changed as a result of a coupon we need to reload if the shipping method panel is on this page */
                if ((w == 'coupon' || w == 'coupon-r') && data['extras']['status'] != 'BAD') {
                  if (data['extras']['shipchange'] == 'y' && $('#panel-ship-method').length) {
                    $('.active-breadcrumb').trigger('click'); /* The breadcrumb with the active class should have the correct stepJump call so we trigger it */
                  }
                  if (w == 'coupon-r') {
                    M.toast( {html: '<p>'+_translate('Your coupon was removed.')+'</p>'} );
                    $('#discount-card, #discount-card-mobile').val(null);
                    $('#discount-card, #discount-card-mobile').blur();
                  }
                  if (w == 'coupon') {
                  M.toast({html: '<p>' + data['extras']['msg'] + '!</p>'});
                    setTimeout(function(){
                      $('.coupon-desc').highlight();
                    }, 300);
                  }
                }
  
                if (w == 'ship-method') {
                  $('.cart-subtotal-row.shipping-desc').highlight();
                } else if (w == 'insurance') {
                  if (postParams['newVal'] == 'y') {
                    setTimeout(function(){ $('.cart-line-item.shipping-insurance').highlight(); }, 200);
                  } else {
                    $('tr.cart-line-item.shipping-insurance div').animate({height: 'toggle', opacity: 'toggle'}, 500);
                  }
                }
                if (typeof data['orderBlock'] !== undefined) { TMPL['ORDERBLOCK'] = data['orderBlock']; _evalOrderBlock(); }
              g4.setSubmitLock(false);
              if (w == 'ship') {
                validationNeeded = false;
              }
              if (data['extras']['sz_check'] && data['extras']['sz_check'] != 'ok') {
                validationNeeded = true;
                _updatePanel('ship');
                if (data['extras']['sz_check'] == 'invalid-zip') {
                  $('#validationError').html(_translate('You did not enter a valid zip code. Please review your entries and try again.'));
                } else {
                  $('#validationError').html(_translate('The state and zip you have provided do not appear to match. Please review your entries and try again.'));
                }
                $('#av_modal_err').modal('open');
              //} else if (data['extras']['validation_error']) {
              } else {
                if (data['extras']['validation_compare']) {
                  if (data['extras']['validation_compare'] == 'different') {
                    validationNeeded = true;
                    _updatePanel('ship');
                    validatedAddress=data['extras']['validation_address'];
                    $('#av_address').html(data['extras']['validation_address_human']);
                    $('#av_modal').modal('open');
                  } else {
                    if (data['extras']['validation_compare'] == 'ziponly') {
                      $('#ship_zip').val(data['extras']['validation_address']['zip_post']);
                    }
                    if (callback) { callback(); }
                  }
                } else {
                    if (callback) { callback(); }
                }
              }
            }
          } else if (data['response'] == 'validation') {
            if (w == 'ship' || w == 'bill') { _validatorHeartbeat('address'); }
            g4.setSubmitLock(false);
            switch (w) {
              case 'ship':
              case 'bill':
                M.toast({html:'<p>'+_translate('Your address does not pass validation. Please check all the required fields.')+'</p>'});
                break;
              case 'email':
                M.toast({html:'<p>'+_translate('Your email address does not appear to be formatted correctly. Please check it for typos.')+'</p>'});
                break;
              case 'ship-method':
                _validatorHeartbeat('ship-method');
                M.toast({html:'<p>'+_translate('Error saving shipping method. Reloading page...')+'</p>'});
                location.reload();
                break;
              default:
                M.toast({html:'<p>'+_translate('There was a problem with your order! Please')+' <a href="mailto:support@leadingedgehealth.com?subject="Validation Issue: Order ' + TMPL['SESSION'] + '">'+_translate('contact us for assistance')+'</a>!</p>'});
                break;
            }
          } else if (data['response'] == 'already-done') {
                      stepJump('thankyou');
          } else if (data['response'] == 'already-done-fnl') {
                      stepJump('fnl');
          } else {
            g4.setSubmitLock(false);
            M.toast({html:'<p>'+_translate('There was a problem with your order! Please')+' <a href="mailto:support@leadingedgehealth.com?subject="Validation Issue: Order ' + TMPL['SESSION'] + '">'+_translate('contact us for assistance')+'</a>!</p>'});
          }
          if (w == 'ship-method' || w == 'upsell' || w == 'insurance') {
            $('#process_modal').modal('close');
          }
        }, 'json').fail(() => {
          _validatorHeartbeat('commerror-saveData');
          g4.setSubmitLock(false);
          $('#alert-content-modal').html("<p>"+_translate('You have lost connection to the order server. Please check your connection and refresh this page.')+"</p>");
          $('#alert_modal').modal('open');
        });
      } else {
        g4.setSubmitLock(false);
        return false;
      }
    }
  
    function saveShipping(w) {
      _saveData('ship-method', function() { _loadCart(); });
    }
  
    function saveInsurance() {
      _saveData('insurance', function() { _loadCart(); });
    }
  
    function saveSMS() {
      _saveData('sms_consent', function() { });
    }
  
    function saveGDPR() {
      _saveData('gdpr', function() { });
    }
  
    function saveLanguage() {
      _saveData('language', function() { _loadCart(); });
    }
  
    function saveCoupon(w) {
      targetCoupon = '';
      if (w == 'mobile') {
        targetCoupon = $('#discount-card-mobile').val();
      } else {
        targetCoupon = $('#discount-card').val();
      }
      if (targetCoupon) {
        _saveData('coupon', function() { _loadCart(); });
      }
    }
  
    function removeCoupon() {
      targetCoupon = '';
      _saveData('coupon-r', function() { _loadCart(); });
    }
  
    function stepJump(target) {
      setSbt(true);
      if (parseInt(target)) {
        window.location.href = TMPL['SELF']+'?step='+target+'&session='+TMPL['SESSION'];
      } else if (target == 'declined' || target == 'thankyou' || target == 'fnl') {
        window.location.replace(TMPL['SELF']+'?step='+target+'&session='+TMPL['SESSION']);
      } else {
        $('body').append($('<form/>').attr({'action': TMPL['SELF'], 'method': 'post', 'id': 'replacer'})
          .append($('<input/>').attr({'type': 'hidden', 'name': 'step', 'value': target}))
          .append($('<input/>').attr({'type': 'hidden', 'name': 'session', 'value': TMPL['SESSION']}))
          .append($('<input/>').attr({'type': 'hidden', 'name': 'oid', 'value': TMPL['OID']}))
        ).find('#replacer').submit();
      }
    }
  
    function _loadCart() {
      var body = ''; var errCartFail;
      var nocache = new Date();
      var postParams = { rm: 'sCart', oid: TMPL['OID'], session: TMPL['SESSION'], nocache: nocache.getTime() };
      $.post(TMPL['SELF'], postParams, function(data) {
        if (data['response'] == 'success') {
          if (data['orderBlock'] == 'true') {
            $('#cart-wrapper .cart-blocked, #cart-wrapper-mobile .cart-blocked').removeClass('hide');
            M.toast( {html: '<p>'+_translate('We cannot ship to your address!')+'</p>'} );
            _renderCartQuantityPrice(true);
          } else {
              var ts = 0;
            $.each(data['products'], function(key, value){
                ts += (parseFloat(value.base_price)*parseInt(value.quantity))-parseFloat(value.price);
              body += _renderCartItem(data['currency_code'],value);
            });
            $('#cart-wrapper .cart, #cart-wrapper-mobile .cart').removeClass('hide');
  
            $('#cart-wrapper table tbody').html(body);
            $('#cart-wrapper-mobile table tbody').html(body);
            if (data['tax'] && data['tax']['validation'] && data['tax']['validation'] == 'ok') {
              validationNeeded = false;
            } else {
              validationNeeded = true;
            }
            _renderCartTotals(data,ts);
            _renderCartQuantityPrice(false, data['currency_code'] + data['totals'].gt_amount);
          }
          if (typeof data['orderBlock'] !== undefined) { TMPL['ORDERBLOCK'] = data['orderBlock']; _evalOrderBlock(); }
          if (data['tax'] && data['tax']['status'] && data['tax']['status'] == 'needed') {
            calcTaxes();
          }
        } else {
          // Couldn't load cart...
          errCartFail = '<p class="center-align">'+_translate('There was a problem loading your cart.')+'</p>';
          $('#cart-wrapper, #cart-wrapper-mobile').html(errCartFail);
          M.toast( {html: errCartFail} );
        }
      }, 'json').fail(() => {
        _validatorHeartbeat('commerror-loadCart');
        $('#alert-content-modal').html("<p>"+_translate('You have lost connection to the order server. Please check your connection and refresh this page.')+"</p>");
        $('#alert_modal').modal('open');
      });
    }
  
    function _renderCartQuantityPrice(blocked, gt) {
      var blockedMsg = blocked ? '<strong>'+_translate('ORDER BLOCKED')+'</strong> - ' : '';
      $('.cart-items-quanitity').html(blockedMsg + ' ' + $('#cart-items tbody tr.cart-line-item').length + ' Item(s)' + (gt ?  ': ' + gt : ''));
    }
  
    function _renderCartItem(c, item) {
      var template = $('#cart-lineItem-template').html();
      template = template.replace(/{product}/g, item.product);
      var desc = item.desc || '';
      if (item.discount_tag) {
        if (desc != '') { desc += "<BR>"; }
        desc += '<span class="cart-line-discount-tag">'+item.discount_tag+'</span>';
      }
      template = template.replace(/{desc}/g, _translate(desc));
      template = template.replace(/{imageURL}/g, item.imageURL);
      if (Number(String(item.price).replace(/[^0-9\.]+/g,"")) <= 0) {
        template = template.replace(/{free}/g, 'price-free');
        template = template.replace(/{price}/g, _translate('FREE!'));
      } else {
        template = template.replace(/{free}/g, '');
        var prc = parseFloat(item.base_price*item.quantity).toFixed(2);
        if (item.savings && parseFloat(item.savings) > 0) {
          //prc += '<span class="cart-line-item-savings"><BR>-'+c+parseFloat(item.savings).toFixed(2)+'</span>';
        }
        var lang = _getLang();
        if (lang == 'de' || lang == 'fr') {
          template = template.replace(/{price}/g,prc.replace(/\./g,',')+c);
        } else {
          template = template.replace(/{price}/g,c+prc);
        }
      }
      template = template.replace(/{quantity}/g, item.quantity>1 ? item.quantity : '');
      template = template.replace(/{type}/g, item.type);
      return template;
    }
  
    function _renderCartTotals(data,totalsavings) {
      var s = data['shipping'];
      var t = data['totals'];
      var c = data['currency_code'];
      var a = data['currency_abbr'];
      var d = data['discount'];
      var euroFormat = false;
      var lang = _getLang();
      if (lang == 'de' || lang == 'fr') {
        euroFormat = true;
      }
      var template = $('#cart-lineTotals-template').html();
      var discount_line = 0;
      var show_discount = false;
      var taxCalculated = false;
  
      template = template.replace(/{clabel-subtotal}/g, _translate('Subtotal'));
      template = template.replace(/{clabel-discount}/g, _translate('Discount'));
      template = template.replace(/{clabel-total}/g, _translate('Total'));
  
      if (parseFloat(t.gt_savings) > 0) {
        show_discount = true;
        discount_line = parseFloat(t.gt_savings);
      }
      if (d.code != '') {
        $('.coupon-wrapper').hide();
        $('.coupon-link').hide();
        show_discount = true;
        if (parseFloat(d.amount) != 0) {
          template = template.replace(/{coupon_code}/g, d.code);
          //discount_line += parseFloat(d.amount);
        } else {
          template = template.replace(/{coupon_code}/g, d.code);
          if (discount_line == 0) {
            template = template.replace(/{discount}/g, '<i>bonus</i>');
          }
        }
      } else {
        $('.coupon-wrapper').hide();
        $('.coupon-link').show();
      }
      if (discount_line > 0) {
        template = template.replace(/{coupon_code}/g, '');
        if (euroFormat) {
          template = template.replace(/{discount}/g, '-'+parseFloat(discount_line).toFixed(2).replace(/\./g,',')+c);
        } else {
          template = template.replace(/{discount}/g, '-'+c+parseFloat(discount_line).toFixed(2));
        }
      }
      if (show_discount) {
        template = template.replace(/{hasDiscount}/g, '');
      } else {
        template = template.replace(/{hasDiscount}/g, 'discount-hide');
      }
      if (data['tax']) {
        if (data['tax']['status'] == 'ineligible') {
          template = template.replace(/{hasTax}/g, 'tax-hide');
          template = template.replace(/{oneTax}/g, 'tax-hide');
          template = template.replace(/{canadaTax}/g, 'tax-hide');
          template = template.replace(/{vatTax}/g, 'tax-hide');
        } else {
          if (data['tax']['type'] == 'standard') {
            template = template.replace(/{canadaTax}/g, 'tax-hide');
            template = template.replace(/{vatTax}/g, 'tax-hide');
          } else if (data['tax']['type'] == 'canada') {
            template = template.replace(/{oneTax}/g, 'tax-hide');
            template = template.replace(/{vatTax}/g, 'tax-hide');
          } else if (data['tax']['type'] == 'eu') {
            template = template.replace(/{canadaTax}/g, 'tax-hide');
            template = template.replace(/{oneTax}/g, 'tax-hide');
          }
          if (data['tax']['status'] == 'pending' || data['tax']['status'] == 'unknown') {
            template = template.replace(/{salestaxes}/g, '-');
            template = template.replace(/{canadaGST}/g, '-');
            template = template.replace(/{canadaPST}/g, '-');
            template = template.replace(/{euVAT}/g, '-');
          } else if (data['tax']['status'] == 'needed') {
            template = template.replace(/{salestaxes}/g, _translate('calculating...'));
            template = template.replace(/{canadaGST}/g, _translate('calculating...'));
            template = template.replace(/{canadaPST}/g, _translate('calculating...'));
            template = template.replace(/{euVAT}/g, _translate('calculating...'));
          } else if (data['tax']['status'] == 'estimated') {
            taxCalculated = true;
            if (euroFormat) {
              template = template.replace(/{salestaxes}/g, parseFloat(data['tax']['sales_tax']).toFixed(2).replace(/\./g,',')+c);
              template = template.replace(/{canadaGST}/g, parseFloat(data['tax']['gst_tax']).toFixed(2).replace(/\./g,',')+c);
              template = template.replace(/{canadaPST}/g, parseFloat(data['tax']['pst_tax']).toFixed(2).replace(/\./g,',')+c);
              template = template.replace(/{euVAT}/g, parseFloat(data['tax']['sales_tax']).toFixed(2).replace(/\./g,',')+c);
            } else {
              template = template.replace(/{salestaxes}/g, c+parseFloat(data['tax']['sales_tax']).toFixed(2));
              template = template.replace(/{canadaGST}/g, c+parseFloat(data['tax']['gst_tax']).toFixed(2));
              template = template.replace(/{canadaPST}/g, c+parseFloat(data['tax']['pst_tax']).toFixed(2));
              template = template.replace(/{euVAT}/g, c+parseFloat(data['tax']['sales_tax']).toFixed(2));
            }
          }
        }
      } else {
        template = template.replace(/{hasTax}/g, 'tax-hide');
        template = template.replace(/{hasTax}/g, 'tax-hide');
        template = template.replace(/{oneTax}/g, 'tax-hide');
        template = template.replace(/{canadaTax}/g, 'tax-hide');
        template = template.replace(/{vatTax}/g, 'tax-hide');
      }
  
      if (taxCalculated) {
        template = template.replace(/{clabel-tax-us}/g, _translate('Estimated Taxes'));
        template = template.replace(/{clabel-tax-ca-gst}/g, _translate('Estimated')+' GST/HST');
        template = template.replace(/{clabel-tax-ca-pst}/g, _translate('Estimated')+' PST/RST/QST');
        template = template.replace(/{clabel-tax-vat}/g, _translate('Estimated')+' VAT');
      } else {
        template = template.replace(/{clabel-tax-us}/g, _translate('Taxes')+' ('+_translate('to be calculated')+')');
        template = template.replace(/{clabel-tax-ca-gst}/g, 'GST/HST ('+_translate('to be calculated')+')');
        template = template.replace(/{clabel-tax-ca-pst}/g, 'PST/RST/QST ('+_translate('to be calculated')+')');
        template = template.replace(/{clabel-tax-vat}/g, 'VAT ('+_translate('to be calculated')+')');
      }
  
      //if (parseFloat(totalsavings).toFixed(2) > 0) {
      //	template = template.replace(/{total_savings}/g, c+parseFloat(totalsavings).toFixed(2));
        //	$('.totalsavings-value').html(c+parseFloat(totalsavings).toFixed(2));
        //} else {
        //	$('.total-savings-wrapper').hide();
        //	$('.total-savings-row').hide();
        //}
      //$('.total-savings-row').show();
  
      if (euroFormat) {
        template = template.replace(/{subtotal}/g, parseFloat(t.st_amount).toFixed(2).replace(/\./g,',')+c);
      } else {
        template = template.replace(/{subtotal}/g, c+parseFloat(t.st_amount).toFixed(2));
      }
      if (s.description) {
        template = template.replace(/{shipping_desc}/g, '' +s.description);
      } else {
        template = template.replace(/{shipping_desc}/g, _translate('Shipping'));
      }
  
      if (s.description.length > 25) {
        template = template.replace(/{tooltipped}/g, 'tooltipped');
      }
  
      if (s.amount == '-') {
        template = template.replace(/{shipping}/g, s.amount);
        template = template.replace(/{free}/g, '');
        template = template.replace(/{freeship}/g, '');
      } else if (parseFloat(s.amount) == 0) {
        template = template.replace(/{shipping}/g, _translate('free'));
        template = template.replace(/{freeship}/g, 'freeshipping');
        template = template.replace(/{free}/g, 'price-free');
      } else {
        if (euroFormat) {
          template = template.replace(/{shipping}/g, parseFloat(s.amount).toFixed(2).replace(/\./g,',')+c);
        } else {
          template = template.replace(/{shipping}/g, c+parseFloat(s.amount).toFixed(2));
        }
        template = template.replace(/{free}/g, '');
        template = template.replace(/{freeship}/g, '');
      }
      // template = template.replace(/{free}/g, 'price-free');
      if (euroFormat) {
        template = template.replace(/{total}/g, parseFloat(t.gt_amount).toFixed(2).replace(/\./g,',')+c);
      } else {
        template = template.replace(/{total}/g, c+parseFloat(t.gt_amount).toFixed(2));
      }
      template = template.replace(/{currency}/g, a.toUpperCase());
  
      $('#cart-wrapper .cart-totals tbody').html(template);
      $('#cart-wrapper-mobile .cart-totals tbody').html(template);
  
      $('#cart-wrapper').addClass('loaded');
  
      $('.icon-remove-coupon').click(function() {
        g4.removeCoupon();
      });
  
      if (show_discount && d.code == '') {
        $('.icon-remove-coupon').hide();
      }
    }
  
    function _primeCountryOptions(type) {
      $('#' + type + '_post_other, #' + type + '_zip').hide();
      $('#' + type + '_aus-wrapper').hide();
      $('#' + type + '_auspost-wrapper').hide();
      $('#' + type + '_other-wrapper, #' + type + '_state-wrapper, #' + type + '_zip-wrapper, #' + type + '_post_other-wrapper').hide();
      $('#' + type + '_other-wrapper, #' + type + '_state-wrapper, #' + type + '_zip-wrapper, #' + type + '_post_other-wrapper').hide();
      $('#' + type + '_other-wrapper, #' + type + '_prov-wrapper, #' + type + '_post-wrapper, #' + type + '_post_other-wrapper').hide();
      $('.disclaimer-international-text').hide();
      $('.disclaimer-domestic-text').hide();
    }
  
    function _selectCountry(method,doSave=false) {
      var type = method ? method : 'ship';
  
      if ($('#' + type + '_country').length) {
        el = $('#' + type + '_country')[0];
        if (el.value == 'ca') {
          _primeCountryOptions(type);
          $('#' + type + '_prov-wrapper, #' + type + '_post-wrapper, #' + type + '_post').show();
          $('.disclaimer-international-text').show();
  
        } else if (el.value == 'au') {
          _primeCountryOptions(type);
          $('#' + type + '_aus-wrapper, #' + type + '_auspost-wrapper').show();
          $('.disclaimer-international-text').show();
  
        } else if (el.value == 'us' || el.value == 'mil') {
          _primeCountryOptions(type);
          $('#' + type + '_state-wrapper, #' + type + '_zip-wrapper, #' + type + '_zip').show();
            $('.disclaimer-domestic-text').show();
  
        } else {
          _primeCountryOptions(type);
          $('#' + type + '_other-wrapper, #' + type + '_post_other-wrapper, #' + type + '_post_other').show();
            $('.disclaimer-international-text').show();
        };
        if (el.value == 'sa') {
          $('#sa_msg').show();
        } else {
          $('#sa_msg').hide();
        }
  
        // Catch autofill-triggered changes...
        // $('#' + type + '_country').material_select();
        // $('#' + type + '_country').formSelect();
  
          if (doSave) {
          _saveData('ship-country', function() { _loadCart(); });
        }
      }
    }
  
    function _toggleBilling(choice) {
      if (choice == 'init') {
        $($('input:radio[name=bill-method]:checked')[0]).click();
        return false;
      } else {
        allPanelsOK = false;
        sameAsShippingToggled = true;
        $('#bill-same-header, #bill-diff-header').removeClass('active');
        if ($('input[name=bill-method]:checked').prop('id') != 'same') {
          // $('#bill-diff-header + .collapsible-body').addClass('active').slideDown();
          var billingPanels = M.Collapsible.getInstance($('.collapsible#billing-collapsible'));
          billingPanels.open(0);
        }
      }
  
    }
  
    function _openContent(obj) {
      $(obj).stop(true).slideDown({ duration: 300, queue: true });
    }
  
    function _closeContent(obj) {
      $(obj).stop(true).slideUp({ duration: 300, queue: true });
    }
  
    function _togglePayment(choice) {
      _closeContent('#payment-method-credit .collapsible-body, #payment-method-paypal .collapsible-body, #payment-method-sofort .collapsible-body');
      switch ($('input[name=pay-method]:checked')[0].id) {
        case 'credit':
          _openContent('#payment-method-credit .collapsible-body');
          _toggleButtons('submit-button');
          break;
        case 'paypal':
          _openContent('#payment-method-paypal .collapsible-body');
          _toggleButtons('submit-button');
          break;
        case 'sofort':
          _openContent('#payment-method-sofort .collapsible-body');
          _toggleButtons('submit-button');
          break;
        default:
          _toggleButtons('submit-button');
          break;
      };
    }
  
    function _toggleButtons(btnID) {
      $('#submit-button').addClass('hide');
      if (btnID != 'na') {$('#' + btnID).removeClass('hide')}
    }
  
    function _checkPanels(callback) {
      var uPanel = '';
      var panelStatus = new Array();
      if ($('#panel-email').length) { panelStatus.push('email'); }
      if ($('#panel-ship-address').length) { panelStatus.push('ship'); }
      if ($('#panel-bill-address').length) { panelStatus.push('bill'); }
      $.each(panelStatus, function(i, v) {
        if (uPanel == '') {
          switch (v) {
            case 'email':
              if ($('#email').val() != $('#email').attr('orig')) { uPanel=v; }
              break;
            case 'ship':
              if (validationNeeded) { uPanel=v; }
            case 'bill':
              if (sameAsShippingToggled) { uPanel=v; sameAsShippingToggled=false; }
              if ($('#'+v+'_salutation').val() != null && $('#'+v+'_salutation').val() != $('#'+v+'_salutation').attr('orig')) { uPanel=v; }
              if ($('#'+v+'_fname').val() != $('#'+v+'_fname').attr('orig')) { uPanel=v; }
              if ($('#'+v+'_lname').val() != $('#'+v+'_lname').attr('orig')) { uPanel=v; }
              if ($('#'+v+'_address').val() != $('#'+v+'_address').attr('orig')) { uPanel=v; }
              if ($('#'+v+'_address_cont').val() != $('#'+v+'_address_cont').attr('orig')) { uPanel=v; }
              if ($('#'+v+'_city').val() != $('#'+v+'_city').attr('orig')) { uPanel=v; }
              if ($('#'+v+'_country').val() != $('#'+v+'_country').attr('orig')) { uPanel=v; }
              if ($('#'+v+'_phone').val() != $('#'+v+'_phone').attr('orig')) { uPanel=v; }
              if ($('#'+v+'_state').val() !== null && $('#'+v+'_state').val() != $('#'+v+'_state').attr('orig')) { uPanel=v; }
              if ($('#'+v+'_zip').val() != $('#'+v+'_zip').attr('orig')) { uPanel=v; }
              if ($('#'+v+'_prov').val() != null && $('#'+v+'_prov').val() != $('#'+v+'_prov').attr('orig')) { uPanel=v; }
              if ($('#'+v+'_post').val() != $('#'+v+'_post').attr('orig')) { uPanel=v; }
              if ($('#'+v+'_other').val() != $('#'+v+'_other').attr('orig')) { uPanel=v; }
              if ($('#'+v+'_post_other').val() != $('#'+v+'_post_other').attr('orig')) { uPanel=v; }
              break;
          } /* switch */
        }
      });
      if (uPanel != '') {
        allPanelsOK = false;
        _saveData(uPanel, function(){_updatePanel(uPanel,function(){_checkPanels(callback)})});
      } else {
        allPanelsOK = true;
        callback();
      }
    }
  
    function _updatePanel(p,callback) {
      switch (p) {
        case 'email':
          $('#email').attr('orig',$('#email').val());
          break;
        case 'ship':
        case 'bill':
          $('#'+p+'_salutation').attr('orig',$('#'+p+'_salutation').val());
          $('#'+p+'_fname').attr('orig',$('#'+p+'_fname').val());
          $('#'+p+'_lname').attr('orig',$('#'+p+'_lname').val());
          $('#'+p+'_address').attr('orig',$('#'+p+'_address').val());
          $('#'+p+'_address_cont').attr('orig',$('#'+p+'_address_cont').val());
          $('#'+p+'_city').attr('orig',$('#'+p+'_city').val());
          $('#'+p+'_country').attr('orig',$('#'+p+'_country').val());
          $('#'+p+'_phone').attr('orig',$('#'+p+'_phone').val());
          $('#'+p+'_state').attr('orig',$('#'+p+'_state').val());
          $('#'+p+'_zip').attr('orig',$('#'+p+'_zip').val());
          $('#'+p+'_prov').attr('orig',$('#'+p+'_prov').val());
          $('#'+p+'_post').attr('orig',$('#'+p+'_post').val());
          $('#'+p+'_other').attr('orig',$('#'+p+'_other').val());
          $('#'+p+'_post_other').attr('orig',$('#'+p+'_post_other').val());
          $('#'+p+'_aus').attr('orig',$('#'+p+'_aus').val());
          $('#'+p+'_auspost').attr('orig',$('#'+p+'_auspost').val());
          break;
      }
      if (callback) { callback(); }
    }
  
    function _evalPayMethod() {
      // cc    => cc + mail (pp. if mblock)
      // pp.ex => pp only
      // na    => cc/pp/mail
  
      switch (TMPL['PAYMENT_METHOD']) {
        case 'cc':
          $('#payment-method-credit').removeClass('hide');
          $('#payment-method-paypal').addClass('hide');
          break;
        case 'pp':
          $('#payment-method-paypal,#payment-method-credit').removeClass('hide');
          break;
        default:
          $('#payment-method-paypal, #payment-method-credit').removeClass('hide');
          break;
        };
  
    }
  
    function _evalOrderBlock() {
      if ($('#panel-payment-method').length) {
        switch (TMPL['ORDERBLOCK']) {
          case 'true':
            $('#payment-collapsible, #billing-collapsible, .billing-header, #terms-collapsible').addClass('hide');
            $('#payment-method-blocked').removeClass('hide');
            $('#payment-method-credit, #payment-method-paypal, #payment-method-sofort').addClass('hide');
            $('#submit-button').addClass('disabled');
            break;
          case 'mail':
            // Treated as "PayPal Only"
            $('#credit').prop('checked', false);
            $('#payment-method-credit').addClass('hide');
            $('#payment-method-sofort').addClass('hide');
            $('#payment-method-blocked').addClass('hide');
            if ($('#payment-method-paypal').length) {
              $('#payment-collapsible').removeClass('hide');
              $('#payment-method-paypal').removeClass('hide');
            } else {
              $('#payment-method-none').removeClass('hide');
              $('#payment-collapsible').removeClass('hide');
              $('#submit-button').addClass('hide');
            }
            break;
          case 'false':
          default:
            break;
        };
      }
    }
  
    function processPPO(ok) {
      // TODO: Retrofit to built-in modal framework
      if (getSubmitLock()) {
        submitBlock();
      } else {
        setSubmitLock(true);
        $('#ppo-offer').modal('close');
        if (ok === true) {
          _saveData('ppo',function(){_processOrder();});
        } else {
          _processOrder();
        }
      }
    }
  
    function processUpsell(w) {
      if (whichUpsell == '') { // means one is already being processed
        whichUpsell = w;
        if ($('#accepted_upsell_inline_'+w).prop("checked") == true) {
          _saveData('upsell', function() { _upsellCleanup('add'); });
        } else {
          _saveData('upsell-r', function() { _upsellCleanup('remove'); });
        }
      }
    }
  
    function _upsellCleanup(x) {
      if (x == 'add') {
        $('#accepted_upsell_inline_'+whichUpsell).prop("checked",true)
      } else {
        $('#accepted_upsell_inline_'+whichUpsell).prop("checked",false)
      }
      whichUpsell = '';
      _loadCart();
    }
  
    function showCoupon() {
      $('.coupon-wrapper').show();
      $('.coupon-link').hide();
    }
  
    function _processOrder() {
      var nocache = new Date();
      var postParams = { rm: 'orderCheck', oid: TMPL['OID'], session: TMPL['SESSION'], nocache: nocache.getTime() };
      if ($('.form').valid() == false) {return false}
      $('#proc_content').html('<p>'+_translate('Verifying order. Please wait...')+'</p>');
      $('#process_modal').modal('open');
      $.post(TMPL['SELF'], postParams, function(data) {
        if (data['passed'] == "1") {
          if ($('#credit').is(':checked')) {
            if (ccMode == 'checkout') {
              Frames.submitCard();
            } else {
              _processNMI();
            }
          } else if ($('#sofort').is(':checked')) {
            _processSofort();
          } else if ($('#paypal').is(':checked')) {
            if ($('#bill_country').val() == 'tr' || $('#ship_country').val() == 'tr') {
              $('#process_modal').modal('close');
              M.toast( {html: '<p>'+_translate('Sorry, we cannot process PayPal orders from Turkey.')+'</p>'} );
              g4.setSubmitLock(false);
            } else {
              _processPayPal();
            }
          }
        } else {
          $('#process_modal').modal('close');
          _validatorHeartbeat('ordercheck');
          if (data['errors'][0] == 'Missing ship method') {
            M.toast( {html: '<p>'+_translate('Please select a method of shipping.')+'</p>'} );
          } else {
            M.toast( {html: '<p>'+_translate('There was a problem with the transaction.')+'</p>'} );
          }
          g4.setSubmitLock(false);
        }
      }, 'json').fail(() => {
        $('#process_modal').modal('close');
        _validatorHeartbeat('commerror-processOrder');
        $('#alert-content-modal').html("<p>"+_translate('Communication Error: Please try again.')+"</p>");
        $('#alert_modal').modal('open');
        g4.setSubmitLock(false);
      });
    }
  
    function _processPayPal() {
      var nocache = new Date();
      var message = '';
      $.post(TMPL['SELF'], { rm: 'ppSessionCheck', oid: TMPL['OID'], session: TMPL['SESSION'], nocache: nocache.getTime() }, function(data) {
        if ((data["status"] == "ready") || (data["status"] == "good")) {
          message = _translate('Transferring you to PayPal. Please wait...');
          if (data["message"] != null) {
            if (data["message"] != "") {
              message = data["message"];
            }
          }
          $('#proc_content').html("<p>" + message + "</p>");
          stepJump('paypal');
        } else if (data["status"] == "not ready") {
          g4.setSubmitLock(false);
          message = _translate('PayPal transaction not ready!');
          if (data["message"] != null) {
            if (data["message"] != "") {
              message = data["message"];
            }
          }
          $('#process_modal').modal('close');
          $('#alert-content-modal').html("<p>" + message + "</p>");
          $('#alert_modal').modal('open');
        } else if (data["status"] == "error") {
          g4.setSubmitLock(false);
          $('#process_modal').modal('close');
          $('#alert-content-modal').html("<p>"+_translate('An error occurred while trying to set up the PayPal transaction. PayPal may be temporarily unavailable. Please try again in a few moments.')+"</p>");
          $('#alert_modal').modal('open');
        } else {
          g4.setSubmitLock(false);
          $('#process_modal').modal('close');
          $('#alert-content-modal').html("<p>"+_translate('An error occurred while trying to set up the PayPal transaction. PayPal may be temporarily unavailable. Please try again in a few moments.')+"</p>");
          $('#alert_modal').modal('open');
        }
      }, 'json').fail(() => {
        _validatorHeartbeat('commerror-processPayPal');
        g4.setSubmitLock(false);
        $('#process_modal').modal('close');
        $('#alert-content-modal').html("<p>"+_translate('Communication Error: Please try again.')+"</p>");
        $('#alert_modal').modal('open');
      });
    }
  
    function _processNMI() {
      var nocache = new Date();
      var blind = ''+$('#billing-cc-number').val().substr(0,6)+$('#billing-cc-number').val().substr($('#billing-cc-number').val().length-4);
      var expiry = ($('#billing_expiry_month').val() + $('#billing_expiry_year').val());
      $.post(TMPL['SELF'], { 'rm': 'nmione', 'oid': TMPL['OID'], 'session': TMPL['SESSION'], 'blind': blind, 'exp': expiry, 'retry': TMPL['NMI_ATTEMPTS'], 'nocache': nocache.getTime() }, function(data) {
        databits = data.split('|');
        if (databits[0] == 'success') {
          $('body').append($('<form/>')
              .attr({'action': databits[1], 'method': 'post', 'id': 'nmi'})
            .append($('<input/>')
              .attr({'type': 'hidden', 'name': 'billing-cc-number', 'value': $('#billing-cc-number').val()}))
            .append($('<input/>')
              .attr({'type': 'hidden', 'name': 'billing-cc-exp', 'value': expiry}))
            .append($('<input/>')
              .attr({'type': 'hidden', 'name': 'billing-cvv', 'value': $('#billing-cvv2').val()})
            )
          ).find('#nmi').submit();
        } else {
          _validatorHeartbeat('preprocess-decline');
          $('#process_modal').modal('close');
          stepJump('declined');
        }
      }).fail(() => {
        g4.setSubmitLock(false);
        _validatorHeartbeat('commerror-processNMI');
        $('#process_modal').modal('close');
        $('#alert-content-modal').html("<p>"+_translate('Communication Error: Please try again.')+"</p>");
        $('#alert_modal').modal('open');
      });
    }
  
    function _validatorHeartbeat(target) {
      var nocache = new Date();
      $.post(TMPL['SELF'], { 'rm': 'validator', 'oid': TMPL['OID'], 'session': TMPL['SESSION'], 'which': target, 'nocache': nocache.getTime() });
    }
  
    function _checkProcessingStatus() {
      var nocache = new Date();
      var checkURL = '';
      var check_proc_type = '';
      var nextStep = '';
      currentAttempts++;
      if (currentAttempts > maxRetries) {
        stepJump('declined');
      } else {
        check_proc_type = '';
        if (TMPL['PPSESSION'] != null && TMPL['PPSESSION'] != "") {
          checkURL = TMPL['SELF']+"?rm=checkppprocessing&oid="+TMPL['OID']+"&session="+TMPL['SESSION']+"&PPSession="+TMPL['PPSESSION']+"&nocache="+nocache.getTime();
        } else if ($('#cs').length && $('#cs').val()) {
          checkURL = TMPL['SELF']+"?rm=checkcdcprocessing&oid="+TMPL['OID']+"&session="+TMPL['SESSION']+"&CheckoutSession="+$('#cs').val()+"&nocache="+nocache.getTime();
        } else {
          checkURL = TMPL['SELF']+"?rm=checkprocessing&oid="+TMPL['OID']+"&session="+TMPL['SESSION']+"&NMISession="+TMPL['NMISESSION']+"&nocache="+nocache.getTime();
        }
        $.getJSON(checkURL, function(data){
          nextStep = '';
          switch (data['res']) {
            case 'success':
              stepJump('thankyou');
              break;
            case 'declined':
              stepJump('declined');
              break;
            case 'declinedSOFORT':
              stepJump('declinedSOFORT');
              break;
            case 'funnel':
              stepJump('fnl');
              break;
            case 'redirect':
              window.location.href = data['redirect'];
              break;
            default:
              setTimeout(function() {_checkProcessingStatus();},retryDelay);
              break;
          } /* switch */
        }).fail(() => {
          _validatorHeartbeat('commerror-checkProcessingStatus');
          setTimeout(function() {_checkProcessingStatus();},retryDelay);
        });
      }
    }
  
    function _checkFunnelProcessingStatus(newsession,newnmisession) {
      var nocache = new Date();
      var nextStep = '';
      currentAttempts++;
      if (currentAttempts > maxRetries) {
        stepJump('thankyou');
      } else {
        var checkURL = TMPL['SELF']+"?rm=checkprocessing&oid="+TMPL['OID']+"&session="+newsession+"&NMISession="+newnmisession+"&nocache="+nocache.getTime();
        $.getJSON(checkURL, function(data){
          nextStep = '';
          switch (data['res']) {
            case 'success':
              stepJump('thankyou');
              break;
            case 'declined':
              stepJump('declined');
              break;
            case 'funnel':
              stepJump('fnl');
              break;
            default:
              setTimeout(function() {_checkFunnelProcessingStatus(newsession,newnmisession);},funnelRetryDelay);
              break;
          } /* switch */
        }).fail(() => {
          _validatorHeartbeat('commerror-checkProcessingStatus');
          setTimeout(function() {_checkFunnelProcessingStatus(newsession,newnmisession);},funnelRetryDelay);
        });
      }
    }
  
    function _submitBrowserData() {
      if ($.ua) {
        var nocache = new Date();
        var postData = { 'rm': 'browserData', 'oid': TMPL['OID'], 'session': TMPL['SESSION'], 'nocache': nocache.getTime() };
        postData.browserName = $.ua.browser.name;
        postData.browserVersion = $.ua.browser.major;
        postData.OSName = $.ua.os.name;
        postData.OSVersion = $.ua.os.version;
        postData.deviceModel = $.ua.device.model;
        postData.deviceType = $.ua.device.type;
        postData.deviceVendor = $.ua.device.vendor;
        postData.screenX = screen.width;
        postData.screenY = screen.height;
        postData.windowX = $(window).width();
        postData.windowY = $(window).height();
        let hostroot = window.location.hostname.split('.').slice(1).join('.');
        //const fpPromise = import('https://fpcdn.io/v3/ZbldbZun5j0bDdHfOiUi').then(FingerprintJS => FingerprintJS.load({endpoint: 'https://fp.'+hostroot}));
        const fpPromise = import('https://fpcdn.io/v3/ZbldbZun5j0bDdHfOiUi').then(FingerprintJS => FingerprintJS.load());
        fpPromise
          .then(fp => fp.get({linkedId: window.location.host}))
          .then(result => {
            postData.fp = result.visitorId;
            $.post(TMPL['SELF'],$.param(postData));
          })
          .catch(error => {
            $.post(TMPL['SELF'],$.param(postData));
          });
      }
      return;
    }
  
    function getSubmitLock() {
      return submitLock;
    }
  
    function setSubmitLock(newVal) {
      if (newVal) {
        //console.log('SubmitLock SET');
      } else {
        //console.log('SubmitLock REMOVED');
      }
      submitLock = newVal;
      return;
    }
  
    function submitBlock() {
      _validatorHeartbeat('submitBlock');
    }
  
    function funnel_accept(qty=false) {
      var nocache = new Date();
      $('#proc_content').html('');
      $('#process_modal').modal('open');
      postData = { 'rm': 'fnl_up', 'oid': TMPL['OID'], 'session': TMPL['SESSION'], 'nocache': nocache.getTime() };
      if (qty !== false) {
        postData['xqty']=qty;
      }
      $.post(TMPL['SELF'], postData, function(data) {
        if (data['success'] && data['NEWSESSION'] && data['NEWNMISESSION']) {
          //stepJump(data['next_step']);
          setTimeout(function() {_checkFunnelProcessingStatus(data['NEWSESSION'],data['NEWNMISESSION']);},funnelRetryDelay);
        } else {
          stepJump('thankyou');
        }
      }, 'json').fail(() => {
        _validatorHeartbeat('commerror-funnel_accept');
        $('#process_modal').modal('close');
        alert(_translate('Communication Error: Please try again.'));
      });
    }
  
    function funnel_preview_accept(qty=false) {
      if (qty) {
        alert("Add to Order: "+qty+" unit(s)");
      } else {
        alert("Add to Order");
      }
    }
  
    function funnel_preview_decline() {
      alert("No Thanks");
    }
  
    function funnel_decline() {
      var nocache = new Date();
      $('#proc_content').html('');
      $('#process_modal').modal('open');
      postData = { 'rm': 'fnl_down', 'oid': TMPL['OID'], 'session': TMPL['SESSION'], 'nocache': nocache.getTime() };
      $.post(TMPL['SELF'], postData, function(data) {
        if (data['success']) {
          if (data['next_step']) {
            stepJump(data['next_step']);
          } else {
            window.location.reload();
          }
        } else {
          window.location.reload();
        }
      }, 'json').fail(() => {
        _validatorHeartbeat('commerror-funnel_decline');
        $('#process_modal').modal('close');
        alert(_translate('Communication Error: Please try again.'));
      });
    }
  
    function calcTaxes() {
      var nocache = new Date();
      $('#proc_content').html('<p>'+_translate('Calculating taxes. Please wait...')+'</p>');
      $('#process_modal').modal('open');
      var postParams = {};
      postParams['rm']      = 'te';
      postParams['oid']     = TMPL['OID'];
      postParams['session'] = TMPL['SESSION'];
      postParams['nocache'] = nocache.getTime();
      $.post(TMPL['SELF'], postParams, function(data) {
        $('#process_modal').modal('close');
        if (data['response'] == 'calculated' || data['response'] == 'ineligible') {
          _loadCart();
        } else if (data['response'] != 'retrieved') {
          errCartFail = '<p class="center-align">'+_translate('A problem occurred while trying to calculate your taxes.')+'</p>';
          $('#cart-wrapper, #cart-wrapper-mobile').html(errCartFail);
          M.toast( {html: errCartFail} );
        }
      }, 'json').fail(() => {
        _validatorHeartbeat('commerror-calcTaxes');
        $('#alert-content-modal').html("<p>"+_translate('You have lost connection to the order server. Please check your connection and refresh this page.')+"</p>");
        $('#alert_modal').modal('open');
      });
    }
  
    function taxValidateChoice(w) {
      $('#av_modal').modal('close');
      switch (w) {
        case 'no':
          allPanelsOK = true;
          _saveData('tvOverride',function(){advance()});
          break;
        case 'yes':
          allPanelsOK = false;
          if ('address1' in validatedAddress) { $('#ship_address').val(validatedAddress['address1']); }
          //if ('address2' in validatedAddress) { $('#ship_address_cont').val(validatedAddress['address2']); } else { $('#ship_address_cont').val(''); }
          if ('city' in validatedAddress) { $('#ship_city').val(validatedAddress['city']); }
          if (validatedAddress['countryCode'] == 'us') {
            if ('region' in validatedAddress) { $('#ship_state').val(validatedAddress['region']).formSelect(); }
            if ('zip_post' in validatedAddress) { $('#ship_zip').val(validatedAddress['zip_post']); }
          } else if (validatedAddress['countryCode'] == 'ca') {
            if ('region' in validatedAddress) { $('#ship_prov').val(validatedAddress['region']).formSelect(); }
            if ('zip_post' in validatedAddress) { $('#ship_post').val(validatedAddress['zip_post']); }
          } else if (validatedAddress['countryCode'] == 'au') {
            if ('region' in validatedAddress) { $('#ship_aus').val(validatedAddress['region']).formSelect(); }
            if ('zip_post' in validatedAddress) { $('#ship_auspost').val(validatedAddress['zip_post']); }
          } else {
            if ('region' in validatedAddress) { $('#ship_other').val(validatedAddress['region']); } else { $('#ship_other').val(''); }
            if ('zip_post' in validatedAddress) { $('#ship_post_other').val(validatedAddress['zip_post']); }
          }
          $('#ship').submit();
          break;
        default:
          break;
      }
    }
  
    function _processCheckout(rawdata) {
      var nocache = new Date();
      $.post(TMPL['SELF'], { 'rm': 'doCheckout', 'oid': TMPL['OID'], 'session': TMPL['SESSION'], 'rawdata': JSON.stringify(rawdata), 'nocache': nocache.getTime() }, function(data) {
        if (data['success']) {
          if (data['checkoutsession']) {
            $('body').append($('<form/>').attr({'action': TMPL['SELF'], 'method': 'post', 'id': 'replacer'})
              .append($('<input/>').attr({'type': 'hidden', 'name': 'step', 'value': 'cdcprocess'}))
              .append($('<input/>').attr({'type': 'hidden', 'name': 'session', 'value': TMPL['SESSION']}))
              .append($('<input/>').attr({'type': 'hidden', 'name': 'CheckoutSession', 'value': data['checkoutsession']}))
              .append($('<input/>').attr({'type': 'hidden', 'name': 'oid', 'value': TMPL['OID']}))
            ).find('#replacer').submit();
          } else if (data['thankyou']) {
            stepJump('thankyou');
          }
        } else {
          _validatorHeartbeat('preprocess-decline');
          stepJump('declined');
        }
      },'json').fail(() => {
        _validatorHeartbeat('commerror-processCheckout');
        stepJump('declined');
      });
    }
  
    function _processSofort() {
      var nocache = new Date();
      $.post(TMPL['SELF'], { 'rm': 'doSofort', 'oid': TMPL['OID'], 'session': TMPL['SESSION'], 'nocache': nocache.getTime() }, function(data) {
        if (data['success']) {
          if (data['checkoutsession']) {
            $('body').append($('<form/>').attr({'action': TMPL['SELF'], 'method': 'post', 'id': 'replacer'})
              .append($('<input/>').attr({'type': 'hidden', 'name': 'step', 'value': 'cdcprocess'}))
              .append($('<input/>').attr({'type': 'hidden', 'name': 'session', 'value': TMPL['SESSION']}))
              .append($('<input/>').attr({'type': 'hidden', 'name': 'CheckoutSession', 'value': data['checkoutsession']}))
              .append($('<input/>').attr({'type': 'hidden', 'name': 'oid', 'value': TMPL['OID']}))
            ).find('#replacer').submit();
          } else if (data['thankyou']) {
            stepJump('thankyou');
          }
        } else {
          _validatorHeartbeat('preprocess-decline');
          stepJump('declined');
        }
      },'json').fail(() => {
        _validatorHeartbeat('commerror-processSofort');
        stepJump('declined');
      });
    }
  
    function initCheckout(pk) {
      var localize = '';
      var lang = _getLang();
      switch (lang) {
        case 'de':
          localize = 'DE-DE';
          break;
        case 'fr':
          localize = 'FR-FR';
          break;
        case 'es':
          localize = 'ES-ES';
          break;
        default:
          localize = 'EN-GB';
          break;
      };
      ccMode = 'checkout';
      Frames.init({
        'publicKey': pk,
        'localization': localize,
        'cardValidationChanged': function(event) {
            checkoutValidated = event.isValid;
          },
        'frameValidationChanged': function(event) {
            switch(event.element) {
              case 'card-number':
                checkoutCardValid = event.isValid;
                break;
              case 'expiry-date':
                checkoutEXPValid = event.isValid;
                break;
              case 'cvv':
                checkoutCVVValid = event.isValid;
                break;
            }
          },
        'cardSubmitted': function() {
          },
        'cardTokenized': function(data) {
            _processCheckout(data);
          },
        'cardTokenizationFailed': function (event) {
            g4.setSubmitLock(false);
            $('#process_modal').modal('close');
            M.toast( {html: '<p>'+_translate('Sorry, we were not able to process your credit card details.')+'</p>'} );
          }
      });
    }
  
  };
  
  (function($) {
    $.extend({
      debounce: function(fn, timeout, invokeAsap, ctx) {
        if(arguments.length == 3 && typeof invokeAsap != 'boolean') {
          ctx = invokeAsap;
          invokeAsap = false;
        }
        var timer;
        return function() {
          var args = arguments;
                ctx = ctx || this;
          invokeAsap && !timer && fn.apply(ctx, args);
          clearTimeout(timer);
          timer = setTimeout(function() {
            !invokeAsap && fn.apply(ctx, args);
            timer = null;
          }, timeout);
        };
      },
      throttle: function(fn, timeout, ctx) {
        var timer, args, needInvoke;
        return function() {
          args = arguments;
          needInvoke = true;
          ctx = ctx || this;
          if(!timer) {
            (function() {
              if(needInvoke) {
                fn.apply(ctx, args);
                needInvoke = false;
                timer = setTimeout(arguments.callee, timeout);
              }
              else {
                timer = null;
              }
            })();
          }
        };
      }
    });
  })(jQuery);
  
  jQuery.fn.highlight = function () {
    $(this).each(function () {
      var el = $(this);
      $("<div/>")
      .width(el.outerWidth())
      .height(el.outerHeight())
      .css({
        "position": "absolute",
        "left": el.offset().left,
        "top": el.offset().top,
        "background-color": "#BEF3EF",
        "opacity": "0.5",
        "z-index": "9999999"
      }).appendTo('body').fadeOut(3500).queue(function () { $(this).remove(); });
    });
  }
  
  function _translate(phrase) {
    if (typeof gen4_localize === "function") {
      return gen4_localize(phrase);
    } else {
      return phrase;
    }
  }
  
  function _getLang() {
    if ($('#choose_language').length) {
      return $('#choose_language').val();
    } else {
      return $('html')[0].lang;
    }
  }