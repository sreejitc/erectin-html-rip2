


$(document).ready(function () {

  $.validator.setDefaults({
    errorClass: 'invalid',
    errorElement: 'div',
    submitHandler: function (form) {
      if (g4.getSubmitLock()) {
        g4.submitBlock();
        return false;
      } else {
        g4.setSubmitLock(true);
        g4.advance();
      }
    }
  });

  $.validator.addMethod('select_check', function(value, element, params) {

    // $('select').material_select();
    $('select').formSelect();

    if ($('#' + element.name).is(":visible") || $('#' + element.name + '-wrapper').is(":visible")) {
      // Item on stage...
      $('.invalid + label:visible:first').attr("tabindex", -1).focus();
      if ($(element).val() == '') {
        return false;
      };
      return (value != null);
    } else {
      // not on stage...
      return true;
    }
  });

  $.validator.addMethod('input_check', function(value, element, params) {
    if ($('#' + element.name).is(":visible")) {
      // Item on stage...
      if (value == '' || value == null) {
        return false;
      } else {
        return true;
      }
    } else {
      // not on stage...
      return true;
    }
  });

  $.validator.addMethod('validate_email', function(value, element) {
    if(/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test( value )) {
      return true;
    } else {
      return false;
  }});

  $.validator.addMethod('zipcodeUS', function(value, element) {
    // Validate US, CA and MX: http://regexr.com/35sb3
    // more specific: /\d{5}-\d{4}$|^\d{5}$|^[a-zA-Z][0-9][a-zA-Z](| )?[0-9][a-zA-Z][0-9]$/.test(value);
    if ($('#' + element.name).is(":visible")) {
      return this.optional(element) || /\d{5}-\d{4}$|^\d{5}$/.test(value);
    } else {
      // not on stage...
      return true;
    }
  });
  $.validator.addMethod('zip', function(value, element) {
    if ($('#' + element.name).is(":visible")) {
      return this.optional(element) || /^[0-9\-]+$/.test(value);
    } else {
      // not on stage...
      return true;
    }
  });

  // http://jqueryvalidation.org/creditcard-method/
  // based on http://en.wikipedia.org/wiki/Luhn_algorithm
  $.validator.addMethod('creditcard', function( value, element ) {
    if ( this.optional( element ) ) {
      return "dependency-mismatch";
    }
    // Accept only spaces, digits and dashes
    if ( /[^0-9 \-]+/.test( value ) ) {
      return false;
    }
    var nCheck = 0,
      nDigit = 0,
      bEven = false,
      n, cDigit;
    value = value.replace( /\D/g, "" );

    // Basing min and max length on
    // http://developer.ean.com/general-info/valid-card-types/
    if ( value.length < 13 || value.length > 19 ) {
      return false;
    }
    for ( n = value.length - 1; n >= 0; n-- ) {
      cDigit = value.charAt( n );
      nDigit = parseInt( cDigit, 10 );
      if ( bEven ) {
        if ( ( nDigit *= 2 ) > 9 ) {
          nDigit -= 9;
        }
      }
      nCheck += nDigit;
      bEven = !bEven;
    }
    return ( nCheck % 10 ) === 0;
  });

  $.validator.addMethod("expiry_check_", function( value, element ) {
    var d = new Date();
    var ud = new Date(parseInt(20 + $('#billing_expiry_year').val()), parseInt($('#billing_expiry_month').val()-1), d.getDay());
    if ($('#billing_expiry_year').val() == null) {
      return true;
    }
    if ($('#credit').prop('checked')) {
      if (value == null) {return false};

      if (ud.getFullYear() <=  d.getFullYear()) {

        if (ud.getMonth() < d.getMonth()) {
          return false;
        }
      }
    }
    return true;
  });

  $.validator.addMethod('cleantext', function(value, element) {
    if ($('#' + element.name).is(":visible")) {
      return this.optional(element) || /^[0-9A-Za-z\u00C0-\u00FF\\\/\#:;&() ,.'-]+$/.test(value);
      return true;
    } else {
      // not on stage...
      return true;
    }
  });
  $.validator.addMethod('cleanname', function(value, element) {
    if ($('#' + element.name).is(":visible")) {
      return this.optional(element) || /^[A-Za-z\u00C0-\u00FF ,.'-]+$/.test(value);
    } else {
      // not on stage...
      return true;
    }
  });

  $.validator.addMethod('clearcountry', function(value, element) {
    if ($('.billmethod#different').is(':checked') && $('#paypal').is(':checked')) {
      if (value != 'tr') {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  });

  $.validator.addMethod('phone', function(value, element) {
    if ($('#' + element.name).is(":visible")) {
      return this.optional(element) || /^[0-9\-\s]+$/.test(value);
    } else {
      // not on stage...
      return true;
    }
  });

  // Credit Card Methods...
  $.validator.addMethod('cc-visa', function(value, element) {
    return this.optional(element) || /^4/.test(value);
  });
  $.validator.addMethod('cc-mc', function(value, element) {
    return this.optional(element) || /^5[1-5]/.test(value);
  });
  $.validator.addMethod('cc-mcevm', function(value, element) {
    // More specific matching:
    // /^5[1-5]\d{2}-?\d{4}-?\d{4}-?\d{4}$|^2(?:2(?:2[1-9]|[3-9]\d)|[3-6]\d\d|7(?:[01]\d|20))-?\d{4}-?\d{4}-?\d{4}$/
    return this.optional(element) || /^[2]\d{1}/.test(value);
  });
  $.validator.addMethod('cc-amex', function(value, element) {
    return this.optional(element) || /^3[47]/.test(value);
  });
  $.validator.addMethod('cc-disc', function(value, element) {
    return this.optional(element) || /^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)/.test(value);
  });


  $(".form").validate({
    success: function(element) {},
    ignore: ":hidden:not(#ship_salutation, #bill_salutation, #ship_state, #bill_state, #ship_country, #bill_country, #ship_prov, #ship_aus, #bill_aus, #bill_prov, #ship_zip, #bill_zip, #ship_postal, #bill_postal, #ship_auspost, #bill_auspost, #ship_post_other, #bill_post_other, #billing_expiry_year, #billing_expiry_month)",
    showErrors: function(errorMap, errorList) {
      this.defaultShowErrors();
    },
    invalidHandler: function(event, validator) {
      var l = ''; var nc = new Date();
      M.toast({
        html: '<p>'+_translate('Please ensure all fields are complete.')+'</p>',
      });
      if ($('#termsagree').length) {
        for (var i in validator.errorMap) {
          l += i + ', ';
        }
        $.post(g4.info()[0], { 'rm': 'validator', 'oid': g4.info()[1], 'session': g4.info()[2], 'which': 'proc-invalid: ' + l, 'nocache': nc.getTime() }, function(data) {});
      }
    },
    errorPlacement: function(error, element) {
      error.appendTo(element.parent());
    },
    rules: {
      email: { rangelength: [3, 70], validate_email: true, required: true },
      ship_salutation: { required: false },
      ship_fname: { required: true, cleanname: true, maxlength: 32 },
      ship_lname: { required: true, cleanname: true, maxlength: 32 },
      ship_address: { required: true, cleantext: true, maxlength: 35 },
      ship_address_cont: { cleantext: true, maxlength: 35 },
      ship_city: { required: true, cleantext: true },
      ship_post: { input_check: true, cleantext: true, rangelength: [6, 7] },
      ship_zip: { input_check: true, rangelength: [5, 10], zip: true, cleantext: true },
      ship_post_other: { input_check: true, rangelength: [0, 10], cleantext: true },
      ship_auspost: { input_check: true, rangelength: [4, 4], zip: true, cleantext: true },
      ship_country: { select_check: true, clearcountry: true },
      ship_state: { select_check: true },
      ship_prov: { select_check: true },
      ship_aus: { select_check: true },
      ship_phone: { input_check: true, rangelength: [10, 15], phone: true },
      ship_method_id: { required: true },
      bill_salutation: { input_check: true, required: false },
      bill_fname: { input_check: true, cleanname: true, maxlength: 32 },
      bill_lname: { input_check: true, cleanname: true, maxlength: 32 },
      bill_address: { input_check: true, cleantext: true, maxlength: 35 },
      bill_address_cont: { cleantext: true, maxlength: 35 },
      bill_city: { input_check: true, cleantext: true },
      bill_country: { select_check: true, clearcountry: true },
      bill_post: { input_check: true, cleantext: true, rangelength: [6, 7] },
      bill_zip: { input_check: true, rangelength: [5, 10], zip: true, cleantext: true },
      bill_post_other: { input_check: true, rangelength: [0, 10], cleantext: true },
      bill_auspost: { input_check: true, rangelength: [4, 4], zip: true, cleantext: true },
      bill_state: { select_check: true },
      bill_prov: { select_check: true },
      bill_aus: { select_check: true },
      bill_phone: { input_check: true, phone: true, rangelength: [10, 15] },
      "bill-method": "required",
      billing_expiry_year: { select_check: true },
      billing_expiry_month: { select_check: true },
      "billing-cc-number": { input_check: true, rangelength: [14, 16], number: true, creditcard: true },
      "billing-cvv2": { input_check: true, rangelength: [3, 4], number: true },
      termsagree: { required: true },
      "pay-method": { required: true },
    },
    messages: {
      email: {
        validate_email: function(){return _translate('A valid email is required.')},
        required:function(){return _translate('An email is required.')},
        rangelength:function(){return _translate('Email must be 3 to 70 characters.')}
      },
      ship_salutation: {
        required:function(){return _translate('Title required.')}
      },
      ship_fname: {
        required: function(){return _translate('First name required.')},
        cleantext: function(){return _translate('Invalid characters.')},
        cleanname: function(){return _translate('Invalid characters.')}
      },
      ship_lname: {
        required: function(){return _translate('Last name required.')},
        cleantext: function(){return _translate('Invalid characters.')},
        cleanname: function(){return _translate('Invalid characters.')}
      },
      ship_address: {
        required: function(){return _translate('Address required.')},
        cleantext: function(){return _translate('Invalid characters.' )}
      },
      ship_address_cont: function(){return _translate('Invalid characters.')},
      ship_city: {
        required: function(){return _translate('City required.')},
        cleantext: function(){return _translate('Invalid characters.')}
      },
      ship_state: function(){return _translate('State required.')},
      ship_post: {
        input_check: function(){return _translate('Postal Code required.')},
        zip: function(){return _translate('Can only contain numbers and dashes.')},
        cleantext: function(){return _translate('Invalid characters.')},
        rangelength:function(){return _translate('Must be between 6 and 7 characters.')}
      },
      ship_zip: {
        input_check: function(){return _translate('ZIP required.')},
        cleantext: function(){return _translate('Invalid characters.')},
        rangelength: function(){return _translate('Must be between 5 and 10 characters.')},
        zip: function(){return _translate('Can only contain numbers and dashes.')},
        zipcodeUS: function(){return _translate('Invalid ZIP Code.')}
      },
      ship_post_other: {
        input_check: function(){return _translate('Postal Code required.')},
        cleantext: function(){return _translate('Invalid characters.')},
        rangelength: function(){return _translate('Must be under 10 characters.')}
      },
      ship_country: {
        required: function(){return _translate('Country required.')},
        clearcountry: function(){return _translate('This country is prohibited.')}
      },
      ship_prov: {
        select_check: function(){return _translate('Province required.')}
      },
      ship_aus: {
        select_check: function(){return _translate('State/Territory required.')}
      },
      ship_auspost: {
        select_check: function(){return _translate('Postal Code required.')},
        zip: function(){return _translate('Can only contain numbers.')}
      },
      ship_phone: {
        input_check: function(){return _translate('A Phone Number is required for courier contact.')},
        phone: function(){return _translate('Phone numbers can only contain numbers (dashes are optional).')},
        rangelength: function(){return _translate('Phone numbers must be between 10 and 15 numbers.')}
      },
      ship_method_id: {
        required: function(){return _translate('Shipping method required.')}
      },
      bill_salutation: {
        select_check: function(){return _translate('Title required.')}
      },
      bill_fname: {
        input_check: function(){return _translate('First name required.')},
        cleantext: function(){return _translate('Invalid characters.')},
        cleanname: function(){return _translate('Invalid characters.')}
      },
      bill_lname: {
        input_check: function(){return _translate('Last name required.')},
        cleantext: function(){return _translate('Invalid characters.')},
        cleanname: function(){return _translate('Invalid characters.')}
      },
      bill_address: {
        input_check: function(){return _translate('Address required.')},
        cleantext: function(){return _translate('Invalid characters.')}
      },
      bill_address_cont: function(){return _translate('Invalid characters.')},
      bill_city: {
        input_check: function(){return _translate('City required.')},
        cleantext: function(){return _translate('Invalid characters.')}
      },
      bill_state: {
        select_check: function(){return _translate('State required.')}
      },
      bill_post: {
        input_check: function(){return _translate('Postal Code required.')},
        cleantext: function(){return _translate('Invalid characters.')},
        zip: function(){return _translate('Can only contain numbers and dashes.')},
        rangelength: function(){return _translate('Must be between 6 and 7 characters.')}
      },
      bill_zip: {
        input_check: function(){return _translate('ZIP required.')},
        cleantext: function(){return _translate('Invalid characters.')},
        rangelength: function(){return _translate('Must be between 5 and 10 characters.')},
        zip: function(){return _translate('Can only contain numbers and dashes.')},
        zipcodeUS: function(){return _translate('Invalid ZIP Code.')},
      },
      bill_post_other: {
        input_check: function(){return _translate('Postal Code required.')},
        cleantext: function(){return _translate('Invalid characters.')},
        rangelength: function(){return _translate('Must be under 10 characters.')},
      },
      bill_country: {
        select_check: function(){return _translate('Country required.')},
        clearcountry: function(){return _translate('This country is prohibited.')},
      },
      bill_prov: function(){return _translate('Province required.')},
      bill_aus: function(){return _translate('State/Territory required.')},
      bill_auspost: {
        select_check: function(){return _translate('Postal Code required.')},
        zip: function(){return _translate('Can only contain numbers.')}
      },
      bill_phone: {
        input_check: function(){return _translate('A Phone Number is required for courier contact.')},
        phone: function(){return _translate('Phone numbers can only contain numbers (dashes are optional).')},
        rangelength: function(){return _translate('Phone numbers must be between 10 and 15 numbers.')}
      },
      "billing-cc-number": {
        input_check: function(){return _translate('Must be a valid credit card number.')},
        rangelength: function(){return _translate('Please enter a number between 14 and 16 digits.')},
        number: function(){return _translate('Please enter numbers only.')},
        creditcard: function(){return _translate('Must be a valid credit card number.')}
      },
      billing_expiry_month: {
        select_check: function(){return _translate('Please select a month.')},
        expiry_check_month: function(){return _translate('Please select a future date.')}
      },
      billing_expiry_year: {
        select_check: function(){return _translate('Please select a year.')}
      },
      //"billing-cvv2": function(){return _translate('CVV2 code required.')}
      "billing-cvv2": {
        input_check: function(){return _translate('CVV2 code required.')},
        rangelength: function(){return _translate('Please enter between 3 and 4 digits.')}
      },
      termsagree: {
        required: function(){return _translate('You must agree to the terms and conditions.')}
      },
      "pay-method": function(){return _translate('You must select a payment method.')}
    }
  });
});